﻿using Game.Common;
using Game.Common.Infrastructure;
using Game.Effects;
using UnityEngine;

/// <summary>
/// Отлавливает столкновения с припятсвием
/// </summary>
public class ObstacleTriger : MonoBehaviour
{
    #region Константы

    /// <summary>
    /// Имя объекта, в который будут складываться эффекты.
    /// </summary>
    private const string RootName = "EffectRoot";

    /// <summary>
    /// Тэг объекта, на который будет применяться эффект.
    /// </summary>
    private const string TargetTag = "Player";

    #endregion

    /// <summary>
    /// Тип действия эффекта.
    /// </summary>
    [SerializeField] 
    private Effects _effectType = Effects.SpeedContinuousEffect;

    /// <summary>
    /// Продолжительность эффекта.
    /// </summary>
    [SerializeField] 
    private float _duration = 0;

    /// <summary>
    /// Значение накладываемого эффекта.
    /// </summary>
    [SerializeField] 
    private float _effectFactor = 0;

    /// <summary>
    /// Ссылка на эффект.
    /// </summary>
    private BaseEffect _effect = null;

    /// <summary>
    /// Ссылка на фабрику эффектов.
    /// </summary>
    private EffectManager _effectManager;

    /// <summary>
    /// Метод инициализации объекта.
    /// </summary>
    private void Start()
    {
        _effectManager = ReferenceAggregator.EffectController;
    }

    /// <summary>
    /// Метод отлавливающий столкновения.
    /// </summary>
    /// <param name="collision">Объект с которым столкнулся</param>
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag(TargetTag))
            return;

        var effectRoot = collision.transform.Find(RootName).gameObject;

        switch (_effectType)
        {
            case Effects.SpeedContinuousEffect:
                _effect = _effectManager.ApplySpeedContinuous(effectRoot, _effectFactor, _duration);
                break;
            case Effects.SpeedTriggerEffect:
                _effect = _effectManager.ApplySpeedTrigger(effectRoot, _effectFactor);
                break;
        }
    }

    /// <summary>
    /// Метод отлавливает выход из столкновения.
    /// </summary>
    /// <param name="collision">Объект с которым столкнулся</param>
    private void OnCollisionExit(Collision collision)
    {
        if (!collision.gameObject.CompareTag(TargetTag))
            return;

        if (_effectType == Effects.SpeedTriggerEffect)
            _effect.CancelEffect();
    }
}