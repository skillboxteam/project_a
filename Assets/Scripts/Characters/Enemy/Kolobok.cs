﻿using Game.Characters.Common;
using Game.Common;
using Game.Common.Infrastructure;
using Game.Controllers;
using Game.Environment.Cells.Description;
using Game.Obstacles;
using UnityEngine;
using UnityEngine.UIElements;

namespace Game.Characters.Enemy
{
    /// <summary>
    /// Компонент колобка.
    /// </summary>
    public class Kolobok : Character
    {
        #region Поля

        /// <summary>
        /// Модель колобка.
        /// </summary>
        [SerializeField, Header("Настройка анимации вращения")]
        private Transform _model = null;

        /// <summary>
        /// Скорость вращения модели.
        /// </summary>
        [SerializeField] 
        private float _rotationRatio = 5.0f;

        /// <summary>
        /// Расстояние обнаружения препятствий.
        /// </summary>
        [SerializeField] 
        private float _detectionDistance = 3.0f;

        #endregion

        #region Методы жизненного цикла

        protected override void Start()
        {
            base.Start();
            CellPosition = ReferenceAggregator.GameController.StartKolobokPos;
        }

        protected override void Update()
        {
            base.Update();
            Run();
            AvoidObstacles();
        }

        #endregion

        #region Остальные методы

        /// <summary>
        /// Выполняет перемещение объекта вперед.
        /// </summary>
        private void Run()
        {
            if (IsRunning && !IsDisabled)
            {
                var delta = CurMoveSpeed * Time.deltaTime;
                MoveForward(delta);
                ModelRotate(delta);
            }
        }
        
        /// <summary>
        /// Выполняет вращение по ходу движения модели.
        /// </summary>
        /// <param name="delta">Значение, на которое необходимо вращать модель.</param>
        private void ModelRotate(float delta)
        {
            _model.Rotate(delta * _rotationRatio, 0, 0);
        }

        /// <summary>
        /// Выполняет обнаружение колайдеров спереди объекта и возвращет информацию о луче.
        /// </summary>
        /// <returns>Информацию о луче обнаружения.</returns>
        private RaycastHit ColliderDetectionForward()
        {
            var rayStartPos = new CellPosition(
                CellPosition.X + DimensionByX / 2.0f,
                CellPosition.Y + DimensionByY,
                CellPosition.Z + DimensionByZ / 2.0f);
            Physics.Raycast(rayStartPos.ToVector3(), CellPosition.Forward.ToVector3(), out var result, _detectionDistance);
            Debug.DrawRay(rayStartPos.ToVector3(), CellPosition.Forward.ToVector3() * _detectionDistance);
            return result;
        }

        /// <summary>
        /// Выполняет обнаружение препятствий и возвращает информацию о найденном.
        /// </summary>
        /// <param name="info">Информация о луче обнаружения.</param>
        /// <returns>Ссылка на объект препятствия.</returns>
        private ObstacleBase ObstacleDetection(RaycastHit info)
        {
            if (info.collider == null)
                return null;

            var obj = info.collider.GetComponent<GameObjectBase>();

            if (obj == null)
                return null;

            if (obj.gameObject.layer == GameController.ObstaclesLayerIndex)
            {
                if (obj is ObstacleBase obs)
                    return obs;
            }

            return null;
        }
        
        /// <summary>
        /// Выполняет обнаружение и уклонение от препятствий.
        /// </summary>
        private void AvoidObstacles()
        {
            if (!IsDead && !IsDisabled)
            {
                var rayInfo = ColliderDetectionForward();
                var obstacle = ObstacleDetection(rayInfo);

                if (obstacle == null)
                    return;
                
                var newPos = ReferenceAggregator.WorldConstructor.GetClosestForwardCell(CellPosition, CellType.CanRun);
                Shift((int)newPos.X);
            }
        }
        
        #endregion
    }
}