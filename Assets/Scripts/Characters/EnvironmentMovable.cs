﻿using Game.Characters.Common;
using Game.Environment.Cells.Description;

namespace Game.Characters
{
    /// <summary>
    /// Компонент для движущихся объектов окружения.
    /// </summary>
    public class EnvironmentMovable : MovableObjectBase
    {
        private CellPosition[] _movePoints;

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            base.Update();
            // todo: обработать перемещение объектамежду точками.
        }
    }
}