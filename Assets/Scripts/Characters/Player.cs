﻿using Game.Characters.Common;
using Game.Common;
using Game.Common.Infrastructure;
using Game.Controllers;
using UnityEngine;

namespace Game.Characters
{
    /// <summary>
    /// Компонент игрока.
    /// </summary>
    public class Player : Character
    {
        #region Методы жизненного цикла

        protected override void Awake()
        {
            base.Awake();
        }
        
        protected override void Start()
        {
            base.Start();
            SwipeController.OnSwipe += CheckInput;
            CellPosition = ReferenceAggregator.GameController.StartPlayerPos;
        }

        protected override void Update()
        {
            base.Update();
            MoveForward();
            CheckInput();
        }

        #endregion
        
        #region Остальные методы

        private void MoveForward()
        {
            var vertical = Input.GetAxis("Vertical");

            if (IsRunning && !IsDisabled)
            {
                if (vertical != 0)
                {
                    var deltaPos = vertical * CurMoveSpeed * Time.deltaTime;
                    MoveForward(deltaPos);
                }
                else if (!ReferenceAggregator.GameController.IsDebugMode)
                {
                    var deltaPos = CurMoveSpeed * Time.deltaTime;
                    MoveForward(deltaPos);
                }
            }
        }
        
        /// <summary>
        /// Управление игроком.
        /// </summary>
        /// <param name="direction">Направление свайпа.</param>
        private void CheckInput(SwipeDirection direction)
        {
            if(IsDisabled)
                return;
            
            if (ReferenceAggregator.GameController.IsDebugMode)
                return;

            switch (direction)
            {
                case SwipeDirection.Left:
                    StartShiftingLaneToLeft();
                    break;
                case SwipeDirection.Right:
                    StartShiftingLaneToRight();
                    break;
                case SwipeDirection.Up:
                    StartJump();
                    break;
                case SwipeDirection.Down:
                    StartSlide();
                    break;
            }
        }
        
        /// <summary>
        /// Управление игроком c клавиш мыши.
        /// </summary>
        private void CheckInput()
        {
            if(IsDisabled)
                return;
            
            if (!ReferenceAggregator.GameController.IsDebugMode)
                return;

            if (Input.GetButtonDown("Fire1"))
                StartShiftingLaneToLeft();
            else
            {
                if (Input.GetButtonDown("Fire2"))
                    StartShiftingLaneToRight();
            }

            if (Input.GetButtonDown("Jump"))
                StartJump();
            if (Input.GetButtonDown("Fire3"))
                StartSlide();
        }
        
        /// <summary>
        /// Начинает смещение в левую полосу.
        /// </summary>
        private void StartShiftingLaneToLeft()
        {
            Shift((int)CellPosition.X - 1);
        }

        /// <summary>
        /// Начинает смещение в правую полосу.
        /// </summary>
        private void StartShiftingLaneToRight()
        {
            Shift((int)CellPosition.X + 1);
        }
        
        #endregion
    }
}