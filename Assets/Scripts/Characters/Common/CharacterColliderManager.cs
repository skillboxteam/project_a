﻿using System.Collections;
using System.Collections.Generic;
using Game.Common;
using Game.Controllers;
using Game.Obstacles;
using UnityEngine;

namespace Game.Characters.Common
{
    public class CharacterColliderManager : MonoBehaviour
    {
        private Character _character;
        
        private void Awake()
        {
            _character = GetComponentInParent<Character>();
        }
        
        /// <summary>
        /// Выполняет обработку входа в зону триггера.
        /// </summary>
        /// <param name="other">Объект триггера.</param>
        protected void OnTriggerEnter(Collider other)
        {
            var obj = other.GetComponent<GameObjectBase>();

            if (obj == null)
                return;

            if (obj.gameObject.layer == GameController.ObstaclesLayerIndex)
            {
                if (obj is ObstacleBase obs)
                {
                    obs.OnCollisionEnterAction?.Invoke(_character);
                }
            }
        }
        
        /// <summary>
        /// Выполняет обработку выхода из зоны триггера.
        /// </summary>
        /// <param name="other">Объект триггера.</param>
        protected void OnTriggerExit(Collider other)
        {
            var obj = other.GetComponent<GameObjectBase>();

            if (obj == null)
                return;

            if (obj.gameObject.layer == GameController.ObstaclesLayerIndex)
            {
                if (obj is ObstacleBase obs)
                {
                    obs.OnCollisionExitAction?.Invoke(_character);
                }
            }
        }
    }
}
