﻿using System;

namespace Game.Characters.Common
{
    [Flags]
    public enum CharacterStates
    {
        Jumping = 1,
        Running = 2,
        Dead = 4,
        Sliding = 8,
        Shifting = 16,
        Drowning = 32,
        Crashing = 64,
        Falling = 128
    }
}