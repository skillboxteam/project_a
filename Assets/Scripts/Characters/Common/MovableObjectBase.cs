﻿using System;
using Game.Common;
using Game.Environment.Cells.Description;
using Game.ForEditor;
using UnityEngine;

namespace Game.Characters.Common
{
    public class MovableObjectBase : GameObjectBase
    {
        #region Поля

        /// <summary>
        /// Скорость перещения без модификаторов.
        /// </summary>
        [SerializeField, Header("Настройка бега")]
        private float _normalMoveSpeed = 0;

        /// <summary>
        /// Макс. скорость перемещения.
        /// </summary>
        [SerializeField] 
        private float _maxMoveSpeed = 50.0f;

        /// <summary>
        /// Минимальная скорость перемещения.
        /// </summary>
        [SerializeField] 
        private float _minMoveSpeed = 0.0f;
        
        /// <summary>
        /// Текущая скорость перещения.
        /// </summary>
        [SerializeField, ReadOnly] 
        private float _curMoveSpeed = 0;
        
        private CharacterAnimator _characterAnimator;
        
        /// <summary>
        /// Множитель скорости анимации бега.
        /// </summary>
        [SerializeField, Header("Настройка анимации")]
        private float _runSpeedMultiplier = 1f;
        
        /// <summary>
        /// Множитель скорости анимации перестроения.
        /// </summary>
        [SerializeField]
        private float _shiftSpeedMultiplier = 1f;
        
        #endregion

        #region Свойства

        protected CharacterAnimator CharacterAnimator => _characterAnimator;

        /// <summary>
        /// Текущая скорость перемещения.
        /// </summary>
        public float CurMoveSpeed
        {
            get => _curMoveSpeed;
            set
            {
                _curMoveSpeed = Mathf.Clamp(value, _minMoveSpeed, _maxMoveSpeed);
                CharacterAnimator.Run(_curMoveSpeed);
                OnCurMoveSpeedChanged?.Invoke(this);
            }
        }

        /// <summary>
        /// Скорость перемещения без модификаторов.
        /// </summary>
        public float NormalMoveSpeed => _normalMoveSpeed;

        /// <summary>
        /// Минимально возможная скорость перемещения.
        /// </summary>
        public float MinMoveSpeed => _minMoveSpeed;
        
        /// <summary>
        /// Максимально возможная скорость перемещения.
        /// </summary>
        public float MaxMoveSpeed => _maxMoveSpeed;
        
        #endregion

        #region События

        /// <summary>
        /// Событие изменения текущей скорости бега.
        /// </summary>
        public Action<MovableObjectBase> OnCurMoveSpeedChanged { get; set; }

        #endregion
        
        #region Методы жизненного цикла

        protected override void Awake()
        {
            base.Awake();
            
            _characterAnimator = new CharacterAnimator(GetComponentInChildren<Animator>(), _runSpeedMultiplier, _shiftSpeedMultiplier);
        }

        protected override void Start()
        {
            base.Start();
            CurMoveSpeed = NormalMoveSpeed;
        }

        protected override void Update()
        {
            base.Update();
        }

        #endregion

        #region Остальные методы

        public void Move(CellPosition to)
        {
            CellPosition = to;
        }

        /// <summary>
        /// Возвращает текущую скорость к нормальному значению.
        /// </summary>
        public void ResetSpeed()
        {
            CurMoveSpeed = NormalMoveSpeed;
        }
        
        #endregion
    }
}