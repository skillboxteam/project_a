﻿using UnityEngine;

namespace Game.Characters.Common
{
    /// <summary>
    /// Класс, отвечающий за анимацию персонажа.
    /// </summary>
    public class CharacterAnimator
    {
        #region Поля

        /// <summary>
        /// Контроллер анимации.
        /// </summary>
        private readonly Animator _animator;
        
        /// <summary>
        /// Множитель скорости анимации бега.
        /// </summary>
        private readonly float _runSpeedMultiplier;
        
        /// <summary>
        /// Множитель скорости анимации перестроения.
        /// </summary>
        private readonly float _shiftSpeedMultiplier;
        
        #endregion

        #region Хеши параметров аниматора

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _startFallTriggerHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _stopFallTriggerHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _bitTriggerHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _strikeTriggerHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _runSpeedHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _jumpTriggerHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _isJumpHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _jumpingSpeedHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _slideTriggerHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _isSlidingHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _shiftLeftTriggerHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _shiftRightTriggerHash;

        /// <summary>
        /// Хэш параметра.
        /// </summary>
        private readonly int _shiftingSpeedHash;

        #endregion

        #region Методы запуска анимаций

        /// <summary>
        /// Запускает анимацию бега.
        /// </summary>
        /// <param name="speed">Скорость бега.</param>
        public void Run(float speed)
        {
            _animator?.SetFloat(_runSpeedHash, speed * _runSpeedMultiplier);
        }

        /// <summary>
        /// Запускает анимацию прыжка.
        /// </summary>
        /// <param name="jumpSpeed">Скорость прыжка.</param>
        public void StartJump(float jumpSpeed)
        {
            _animator.SetBool(_isJumpHash, true);
            _animator.SetFloat(_jumpingSpeedHash, jumpSpeed);
            _animator.SetTrigger(_jumpTriggerHash);
        }
        
        /// <summary>
        /// Запускает анимацию завершения прыжка.
        /// </summary>
        public void StopJump()
        {
            _animator.SetBool(_isJumpHash, false);
            // todo: запускать анимацию завершения прыжка.
        }

        /// <summary>
        /// Запускает анимацию смещения влево.
        /// </summary>
        /// <param name="speed">Скорость смещения.</param>
        public void ShiftLeft(float speed)
        {
            _animator.SetFloat(_shiftingSpeedHash, speed * _shiftSpeedMultiplier);
            _animator.SetTrigger(_shiftLeftTriggerHash);
        }

        /// <summary>
        /// Запускает анимацию смещения вправо.
        /// </summary>
        /// <param name="speed">Скорость смещения.</param>
        public void ShiftRight(float speed)
        {
            _animator.SetFloat(_shiftingSpeedHash, speed * _shiftSpeedMultiplier);
            _animator.SetTrigger(_shiftRightTriggerHash);
        }

        /// <summary>
        /// Запускает анимацию скольжения.
        /// </summary>
        public void StartSlide()
        {
            _animator.SetBool(_isSlidingHash, true);
            _animator.SetTrigger(_slideTriggerHash);
        }
        
        /// <summary>
        /// Завершает анимацию скольжения.
        /// </summary>
        public void StopSlide()
        {
            _animator.SetBool(_isSlidingHash, false);
        }

        /// <summary>
        /// Запускает анимацию смерти персонажа.
        /// </summary>
        public void Dead()
        {
            // todo: добавить анимацию смерти.
        }

        /// <summary>
        /// Запускает анимацию утопления.
        /// </summary>
        public void StartDrowning()
        {
            // todo: добавить анимацию старта утопления.
        }

        /// <summary>
        /// Завершает анимацию утопления.
        /// </summary>
        public void StopDrowning()
        {
            // todo: добавить анимацию окончания утопления.
        }
        
        /// <summary>
        /// Запускает анимацию удара о препятствия.
        /// </summary>
        public void StartCrash()
        {
            _animator.SetTrigger(_strikeTriggerHash);
        }
        
        /// <summary>
        /// Завершает анимацию удара о препятствие.
        /// </summary>
        public void StopCrash()
        {
            // todo: добавить анимацию выхода из состояния оглушения.
        }
        
        /// <summary>
        /// Запускает анимацию падения.
        /// </summary>
        public void StartFall()
        {
            _animator.SetTrigger(_startFallTriggerHash);
        }

        /// <summary>
        /// Завершает анимацию падения.
        /// </summary>
        public void StopFall()
        {
            _animator.SetTrigger(_stopFallTriggerHash);
        }

        #endregion

        #region Конструктор

        public CharacterAnimator(Animator animator, float runSpeedMultiplier, float shiftSpeedMultiplier)
        {
            _startFallTriggerHash = Animator.StringToHash("StartFallTrigger");
            _stopFallTriggerHash = Animator.StringToHash("StopFallTrigger");
            _bitTriggerHash = Animator.StringToHash("BitTrigger");
            _strikeTriggerHash = Animator.StringToHash("StrikeTrigger");
            _runSpeedHash = Animator.StringToHash("RunSpeed");
            _jumpTriggerHash = Animator.StringToHash("JumpTrigger");
            _isJumpHash = Animator.StringToHash("IsJump");
            _jumpingSpeedHash = Animator.StringToHash("JumpingSpeed");
            _slideTriggerHash = Animator.StringToHash("SlideTrigger");
            _isSlidingHash = Animator.StringToHash("IsSliding");
            _shiftLeftTriggerHash = Animator.StringToHash("ShiftLeftTrigger");
            _shiftRightTriggerHash = Animator.StringToHash("ShiftRightTrigger");
            _shiftingSpeedHash = Animator.StringToHash("ShiftingSpeed");
            _animator = animator;
            _runSpeedMultiplier = runSpeedMultiplier;
            _shiftSpeedMultiplier = shiftSpeedMultiplier;
        }

        #endregion
    }
}