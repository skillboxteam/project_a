﻿using System;
using System.Collections;
using Game.Common;
using Game.Common.Infrastructure;
using Game.Controllers;
using Game.Environment.Cells.Description;
using Game.ForEditor;
using Game.Obstacles;
using UnityEngine;

namespace Game.Characters.Common
{
    public class Character : MovableObjectBase
    {
        #region Поля
        
        /// <summary>
        /// Стартовое здоровье.
        /// </summary>
        [SerializeField, Header("Настройка хп")]
        private float _startHealth = 0;

        /// <summary>
        /// Максимальное здоровье.
        /// </summary>
        [SerializeField]
        private float _maxHealth = 10;

        /// <summary>
        /// Текущее здоровье.
        /// </summary>
        [SerializeField, ReadOnly]
        private float _curHealth;
        
        [SerializeField, Header("Настройка прыжков")]
        private float _jumpSpeed = .5f;

        [SerializeField] 
        private float _jumpHeight = 2.0f;

        [SerializeField]
        private float _jumpLengthRatio = .5f;
        
        /// <summary>
        /// Скорость смещения.
        /// </summary>
        [SerializeField, Header("Настройка смещения")]
        private float _shiftSpeed = 5.0f;
        
        /// <summary>
        /// Множитель расстояния в подкате.
        /// </summary>
        [SerializeField, Header("Настройка подкатов")]
        private float _slideLengthRatio = 1.0f;
        
        /// <summary>
        /// Время падения.
        /// </summary>
        [SerializeField, Header("Настройка задержки после падения")]
        private float _fallTimeInSec = 1.0f;
        
        /// <summary>
        /// Времени задержки после столкновения с крупным припятствием.
        /// </summary>
        [SerializeField, Header("Настройка задержки после столкновения")]
        private float _crashTimeInSec = 1.0f;
        
        [SerializeField]
        private CharacterStates _characterStates;

        /// <summary>
        /// Компонент RigidBody.
        /// </summary>
        private Rigidbody _rigidBody = null;
        
        #endregion

        #region Свойства

        /// <summary>
        /// Компонент RigidBody.
        /// </summary>
        public Rigidbody RigidBody
        {
          get
          {
            if (_rigidBody == null)
              _rigidBody = GetComponentInChildren<Rigidbody>();

            return _rigidBody;
          }
        }
        
        /// <summary>
        /// Текущее количество здоровья.
        /// </summary>
        public float CurHealth
        {
          get => _curHealth;
          set
          {
            _curHealth = value;

            if (_curHealth <= 0)
            {
              _curHealth = 0;
              Dead();
            }
            else
            {
              if (_curHealth > MaxHealth)
                _curHealth = MaxHealth;
            }
            
            OnHealthChanged?.Invoke();
          }
        }

        /// <summary>
        /// Минимальное количество здоровья.
        /// </summary>
        public float MaxHealth => _maxHealth;

        public bool IsRunning
        {
          get => _characterStates.HasFlag(CharacterStates.Running);
          set
          {
            if (value)
            {
              _characterStates |= CharacterStates.Running;
              CharacterAnimator.Run(CurMoveSpeed);
            }
            else
            {
              _characterStates ^= CharacterStates.Running;
            }
          }
        }

        public bool IsJumping
        {
          get => _characterStates.HasFlag(CharacterStates.Jumping);
          set
          {
            if (value)
            {
              _characterStates |= CharacterStates.Jumping;
              CharacterAnimator.StartJump(_jumpSpeed);
            }
            else
            {
              _characterStates ^= CharacterStates.Jumping;
              CharacterAnimator.StopJump();
            }
            
            RigidBody.useGravity = !IsJumping;
          }
        }

        public bool IsDead
        {
          get => _characterStates.HasFlag(CharacterStates.Dead);
          set
          {
            if (value)
            {
              _characterStates |= CharacterStates.Dead;
              CharacterAnimator.Dead();
            }
            else
            {
              _characterStates ^= CharacterStates.Dead;
            }
          }
        }

        public bool IsSliding
        {
          get => _characterStates.HasFlag(CharacterStates.Sliding);
          set
          {
            if (value)
            {
              _characterStates |= CharacterStates.Sliding;
              IsJumping = false;
              CharacterAnimator.StartSlide();
            }
            else
            {
              _characterStates ^= CharacterStates.Sliding;
              CharacterAnimator.StopSlide();
            }
          }
        }

        public bool IsShifting
        {
          get => _characterStates.HasFlag(CharacterStates.Shifting);
          set
          {
            if (value)
            {
              _characterStates |= CharacterStates.Shifting;
            }
            else
            {
              _characterStates ^= CharacterStates.Shifting;
            }
          }
        }

        public bool IsDrowning
        {
          get => _characterStates.HasFlag(CharacterStates.Drowning);
          set
          {
            if (value)
            {
              _characterStates |= CharacterStates.Drowning;
              CharacterAnimator.StartDrowning();
            }
            else
            {
              _characterStates ^= CharacterStates.Drowning;
              CharacterAnimator.StopDrowning();
            }
          }
        }

        public bool IsCrashing
        {
          get => _characterStates.HasFlag(CharacterStates.Crashing);
          set
          {
            if (value)
            {
              _characterStates |= CharacterStates.Crashing;
              CharacterAnimator.StartCrash();
            }
            else
            {
              _characterStates ^= CharacterStates.Crashing;
              CharacterAnimator.StopCrash();
            }

            IsRunning = !value;
          }
        }

        public bool IsFalling
        {
          get => _characterStates.HasFlag(CharacterStates.Falling);
          set
          {
            if (value)
            {
              _characterStates |= CharacterStates.Falling;
              CharacterAnimator.StartFall();
            }
            else
            {
              _characterStates ^= CharacterStates.Falling;
              CharacterAnimator.StopFall();
            }

            IsRunning = !value;
          }
        }

        /// <summary>
        /// Пометка, возвращающая true, если персонаж не может выполнять действия (мертв, тонет, оглушен после удара, упал)
        /// </summary>
        protected bool IsDisabled => IsDead || IsDrowning || IsCrashing || IsFalling;

        #endregion

        #region События

        /// <summary>
        ///  Событие изменения здоровья.
        /// </summary>
        public Action OnHealthChanged { get; set; }

        /// <summary>
        /// Событие отмены эффекта.
        /// </summary>
        public Action OnEffectCanceled { get; set; }
        
        #endregion
        
        #region Методы изменения состояния

        private void StartRun()
        {
          if (IsRunning)
            return;

          IsRunning = true;
          // todo: плавное ускорение
        }

        protected void Shift(int lane)
        {
          if (IsShifting || IsDisabled ^ IsCrashing)
            return;

          _endShiftXPos = WorldConstructor.GetValidLaneNumber(lane);

          if (WorldConstructor.IsCanReplaceCharacterToCell(this,
            new CellPosition(_endShiftXPos, Mathf.Round(CellPosition.Y), Mathf.Round(CellPosition.Z))))
          {
            IsShifting = true;
            _shiftLength = Mathf.Abs(_endShiftXPos - CellPosition.X);
            _shiftStep = 0;
            
            if (!IsJumping && !IsSliding)
            {
              if (_endShiftXPos < CellPosition.X)
                CharacterAnimator.ShiftLeft(_shiftSpeed);
              else if (_endShiftXPos > CellPosition.X)
                CharacterAnimator.ShiftRight(_shiftSpeed);
            }
          }
        }

        protected void StartSlide()
        {
          if (IsSliding || IsDisabled)
            return;

          StopJump();
          IsSliding = true;
          _slideLength = _slideLengthRatio * CurMoveSpeed;
          _endSlideZPos = CellPosition.Z + _slideLength;
        }

        private void StopSlide()
        {
          if (!IsSliding)
            return;

          IsSliding = false;
        }

        protected void StartJump()
        {
          if (IsJumping || IsDisabled)
            return;

          IsJumping = true;
          StopSlide();
          _startJumpYPos = CellPosition.Y;
          _middleJumpYPos = _startJumpYPos + (_jumpHeight * 2.0f);
          _endJumpYPos = _startJumpYPos;
          _jumpStep = 0;
          _jumpLength = _jumpLengthRatio * CurMoveSpeed;
        }

        private void StopJump()
        {
          if (!IsJumping)
            return;

          IsJumping = false;
          StartCoroutine(StoppingJump());
        }
        
        /// <summary>
        /// Выполняет прерывание прыжка.
        /// </summary>
        /// <returns>Ожидание конца фрейма.</returns>
        private IEnumerator StoppingJump()
        {
          var stepNumber = 0.0f;

          while (stepNumber <= 1.0f)
          {
            var step = (_jumpSpeed * 3 / (CellPosition.Y - _endJumpYPos)) * Time.deltaTime;
            stepNumber += step;
            CellPosition = new CellPosition(CellPosition.X, Mathf.Lerp(CellPosition.Y, _endJumpYPos, stepNumber), CellPosition.Z);
            yield return new WaitForEndOfFrame();
          }

          CellPosition = new CellPosition(CellPosition.X, _endJumpYPos, CellPosition.Z);
        }

        /// <summary>
        /// Запускает процесс утопления персонажа.
        /// </summary>
        private void StartDrown()
        {
          if (IsDrowning)
            return;

          IsDrowning = true;
        }

        /// <summary>
        /// Завершает процесс утопления персонажа.
        /// </summary>
        private void StopDrown()
        {
          if (!IsDrowning)
            return;

          IsDrowning = false;
        }

        /// <summary>
        /// Запускает анимацию смерти персонажа.
        /// </summary>
        protected void Dead()
        {
          if (IsDead)
            return;

          IsDead = true;
        }

        /// <summary>
        /// Запускает анимацию удара о препятствие.
        /// </summary>
        private void StartCrash()
        {
          if (IsCrashing)
            return;

          IsCrashing = true;
          StartCoroutine(Crashing());
        }

        /// <summary>
        /// Выполняет запуск анимации столкновения и перемещения персонажа на свободную дорожку.
        /// </summary>
        /// <returns>Ожидание времени, после которого персонаж может продолжать путь _timeToRunAfterCrashinSec.</returns>
        private IEnumerator Crashing()
        {
          if (!IsDead && IsCrashing)
          {
            yield return new WaitForSeconds(_crashTimeInSec);
            var newPos = ReferenceAggregator.WorldConstructor.GetClosestForwardCell(CellPosition, CellType.CanRun);
            Shift((int) newPos.X);
            IsCrashing = false;
          }
        }

        public void StartFalling(float length)
        {
          if (IsFalling)
            return;

          IsFalling = true;
          StartCoroutine(Falling(length));
        }

        /// <summary>
        /// Выполняет перемещение во время падения.
        /// </summary>
        /// <param name="fallLength">Общая длина падения.</param>
        /// <returns>Перечислитель.</returns>
        private IEnumerator Falling(float fallLength)
        {
          if (!IsFalling)
            yield return null;
          
          var step = 0.0f;
          var endFallZPos = CellPosition.Z + fallLength;

          while (step < _fallTimeInSec)
          {
            CellPosition = new CellPosition(CellPosition.X, 
              CellPosition.Y, 
              Mathf.Lerp(CellPosition.Z, endFallZPos, step));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
          }

          CellPosition = new CellPosition(CellPosition.X, CellPosition.Y, endFallZPos);
          IsFalling = false;
        }

        #endregion

        #region Остальные методы

        /// <summary>
        /// Перемещает персонажа вперед.
        /// </summary>
        /// <param name="delta">Значение изменения коориднаты по оси Z.</param>
        protected void MoveForward(float delta)
        {
          Move(new CellPosition(CellPosition.X, CellPosition.Y, CellPosition.Z + delta));
        }

        /// <summary>
        /// Запускает процесс столкновения персонажа с препятствием.
        /// </summary>
        /// <param name="damage">Урон персонажу.</param>
        public void Crash(float damage)
        {
          CurHealth -= damage;
          StartCrash();
        }

        #endregion

        #region Методы жизненного цикла

        protected override void Awake()
        {
          base.Awake();
        }

        protected override void Start()
        {
          base.Start();
          CurHealth = _startHealth;
          IsRunning = true;
        }

        protected override void Update()
        {
          base.Update();

          if (!IsDead)
          {
            if (IsJumping)
            {
              _jumpStep += _jumpSpeed / _jumpLength * Time.deltaTime;
              CellPosition = new CellPosition(CellPosition.X,
                MathHelper.Bezier(_startJumpYPos, _middleJumpYPos, _endJumpYPos, _jumpStep), CellPosition.Z);

              if (CellPosition.Y == _endJumpYPos)
                IsJumping = false;
            }

            if (IsShifting)
            {
              _shiftStep += _shiftSpeed / _shiftLength * Time.deltaTime;
              CellPosition = new CellPosition(Mathf.Lerp(CellPosition.X, _endShiftXPos, _shiftStep), CellPosition.Y, CellPosition.Z);

              if (CellPosition.X == _endShiftXPos)
                IsShifting = false;
            }

            if (IsSliding)
            {
              if (CellPosition.Z >= _endSlideZPos)
                StopSlide();
            }
          }
        }

        #endregion
        
        #region Вспомогательные математические поля

        /// <summary>
        /// Значение координаты Y в момент старта прыжка.
        /// </summary>
        private float _startJumpYPos;

        /// <summary>
        /// Среднее опорное значение координаты Y.
        /// </summary>
        private float _middleJumpYPos;

        /// <summary>
        /// Значение координаты Y в момент окончания прыжка.
        /// </summary>
        private float _endJumpYPos;

        /// <summary>
        /// Номер итерации итерполяции значения координаты Y во время прыжка.
        /// </summary>
        private float _jumpStep = 0;

        /// <summary>
        /// Дистанция прыжка.
        /// </summary>
        private float _jumpLength;
        
        /// <summary>
        /// Значение координаты X в момент окончания смещения.
        /// </summary>
        private float _endShiftXPos;

        /// <summary>
        /// Номер итерации итерполяции значения координаты X во время смещения.
        /// </summary>
        private float _shiftStep = 0;
        
        /// <summary>
        /// Дистанция смещения.
        /// </summary>
        private float _shiftLength = 0;
        
        /// <summary>
        /// Дистанция подката.
        /// </summary>
        private float _slideLength;

        /// <summary>
        /// Значение координаты Z в момент окончания подката.
        /// </summary>
        private float _endSlideZPos;
        
        #endregion
    }
}