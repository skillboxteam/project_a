﻿using Game.Characters.Common;
using Game.Common;
using Game.Common.Infrastructure;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    /// <summary>
    /// Управление UI
    /// </summary>
    public class UIManager : MonoBehaviour
    {
        /// <summary>
        /// Статическая ссылка на объект для более простого доступа.
        /// </summary>
        public static UIManager Instance { get; private set; }

        /// <summary>
        /// Текстовое поле скорости.
        /// </summary>
        [SerializeField]
        private Text speedText = null;

        /// <summary>
        /// Текстовое поле счетчика времени действия эффекта.
        /// </summary>
        [SerializeField]
        private Text speedDuration = null;

        /// <summary>
        /// Установить значение скорости в текстовое поле.
        /// </summary>
        /// <param name="movable">Объект скорость которого выводим</param>
        public void SetSpeed(MovableObjectBase movable)
        {
            float speed = movable.CurMoveSpeed;
            speedText.text = speed.ToString("0.0");
            if (speed > 10)
                speedText.color = Color.red;
        }

        /// <summary>
        /// Вывод времени действия эффекта.
        /// </summary>
        /// <param name="duration">длительность эффекта</param>
        public void EffectTimeSUpdate(float duration)
        {
            speedDuration.text = duration.ToString();
        }

        /// <summary>
        /// Выполняет инициализацию объекта.
        /// </summary>
        private void Awake()
        {
            // Проверяем существование экземпляра
            if (Instance == null)
            { // Экземпляр менеджера не найден
                Instance = this; // Задаем ссылку на экземпляр объекта
            }
            else if (Instance == this)
            { // Экземпляр объекта уже существует на сцене
                Destroy(gameObject); // Удаляем объект
            }

            ReferenceAggregator.Player.OnCurMoveSpeedChanged += SetSpeed;
        }
    }
}