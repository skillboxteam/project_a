﻿using System;
using System.Collections;
using Game.UI;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Таймер
    /// </summary>
    public class Timer : MonoBehaviour
    {
        /// <summary>
        /// Продолжительность действия таймера.
        /// </summary>
        private float _duration;

        /// <summary>
        /// Таймер закончил свою работу.
        /// </summary>
        public event Action TimerExpired;

        /// <summary>
        /// Таймер закончил свою работу.
        /// </summary>
        public event Action<float> OnTick;

        #region Методы
        
        /// <summary>
        /// Таймер до окончания.
        /// </summary>
        /// <returns>Объект типа IEnumerator, который необходим для корутины.</returns>
        private IEnumerator Сountdown()
        {
            OnTick += UIManager.Instance.EffectTimeSUpdate;
            
            for (var i = _duration; i > -1; i--)
            {
                yield return new WaitForSeconds(1);
                OnTick?.Invoke(i);
            }

            TimerExpired.Invoke();
            TimerExpired = null;
            OnTick = null;
        }

        /// <summary>
        /// Запустить таймер.
        /// </summary>
        /// <param name="duration">Продолжительность в секундах</param>
        public void StarTimer(float duration)
        {
            _duration = duration;
            StartCoroutine(Сountdown());
        }
        #endregion
    }
}