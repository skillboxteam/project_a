﻿using System;
using UnityEngine.Events;

namespace Game
{
    /// <summary>
    /// Заглушка для событий принимающих float.
    /// </summary>
    [Serializable]
    public class FloatEvent : UnityEvent<float> 
    { 
    }
}
