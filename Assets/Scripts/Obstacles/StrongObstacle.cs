using System;
using Game.Characters.Common;
using Game.Common.Infrastructure;
using UnityEngine;

namespace Game.Obstacles
{
    /// <summary>
    /// Крупное, крепкое препятствие.
    /// </summary>
    public class StrongObstacle : ObstacleBase
    {
        /// <summary>
        /// На сколько процентов снизить скорость.
        /// </summary>
        [SerializeField, Range(0, 1), Header("Урон скорости, %:")]
        private float _slowRatio = .5f;

        /// <summary>
        /// На сколько процентов снизить скорость.
        /// </summary>
        [SerializeField, Header("Время действия урона скорости:")]
        private float _duration = 3;

        /// <summary>
        /// Пометка, увивает ли препятствие персонажа сразу.
        /// </summary>
        [SerializeField] private bool _isKilling = false;

        /// <summary>
        /// Действие, выполняемое при столкновении персонажа с препятствием.
        /// </summary>
        public override Action<Character> OnCollisionEnterAction
        {
            get
            {
                return character =>
                {
                    character.CellPosition = character.CellPosition.Truncate();

                    if (_isKilling)
                        character.Crash(int.MaxValue);
                    else
                    {
                        character.Crash(Damage);
                        ReferenceAggregator.EffectController.ApplySpeedContinuous(-_slowRatio * 100,
                            _duration); // минус для дебафа, 100%
                    }
                };
            }
        }

        /// <summary>
        /// Действие, выполняемое после выхода персонажа с зоны препятствия.
        /// </summary>
        public override Action<Character> OnCollisionExitAction => null;
    }
}