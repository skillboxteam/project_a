﻿using System.Collections.Generic;
using Game.Common;
using Game.Common.Infrastructure;
using Game.Environment.Cells.Description;
using Game.ForEditor;
using UnityEngine;

namespace Game.Obstacles.Management
{
    /// <summary>
    /// Контроллер создания препятствий.
    /// </summary>
    public class ObstaclesSpawnController : MonoBehaviour
    {
        #region Поля

        /// <summary>
        /// Массив пулов.
        /// </summary>
        /// <remarks>
        /// где [0] - простейшие препятствия
        /// </remarks>
        [SerializeField]
        private ObstaclesPoolsGroup[] _pools = null;

        /// <summary>
        /// Стартовый уровень сложности.
        /// </summary>
        [SerializeField]
        private int _startLevel = 0;

        /// <summary>
        /// Уровень текущего препятствия.
        /// </summary>
        [SerializeField, ReadOnly]
        private int _curLevel;

        /// <summary>
        /// Количество созданных препятствий.
        /// </summary>
        [SerializeField, ReadOnly]
        private int _createdObstacleCount;

        /// <summary>
        /// Максимальное количество препятствий на сцене.
        /// </summary>
        [SerializeField]
        private int _maxObstacleOnScene = 10;

        /// <summary>
        /// Количество препятствий для перехода на след. уровень.
        /// </summary>
        [SerializeField]
        private int _nextLevelObstacleCount = 15;

        /// <summary>
        /// Дистанция до препятствия.
        /// </summary>
        [SerializeField]
        private float _distanceToObstacle = 5.0f;

        /// <summary>
        /// Последнее созданное препятствие.
        /// </summary>
        private float _lastPositionObstacle = 0;

        /// <summary>
        /// Разрешение к созданию.
        /// </summary>
        [SerializeField]
        private bool _canSpawn = true;

        /// <summary>
        /// Текущее количество препятствий на сцене.
        /// </summary>
        private int _curObstacleOnScene = 0;

        /// <summary>
        /// Очередь созданных препятствий.
        /// </summary>
        private Queue<ObstacleBase> _createdObstacles;

        #endregion

        #region Методы жизненного цикла

        /// <summary>
        /// Метод инициализации.
        /// </summary>
        private void Start()
        {
            _curLevel = _startLevel;
            _createdObstacleCount = 0;
            _createdObstacles = new Queue<ObstacleBase>();
            _lastPositionObstacle = ReferenceAggregator.Player.CellPosition.Z + _distanceToObstacle;
        }

        /// <summary>
        /// Метод обработки фрейма.
        /// </summary>
        private void Update()
        {
            if (_canSpawn && _curObstacleOnScene < _maxObstacleOnScene)
                CreateObstacle();

            if (ReferenceAggregator.WorldConstructor.IsNeedDestroyObject(_createdObstacles.Peek()))
                DisableObstacle();
        }

        #endregion

        #region Остальные методы

        /// <summary>
        /// Метод создания препятствия.
        /// </summary>
        private void CreateObstacle()
        {
            _createdObstacleCount++;

            if (_createdObstacleCount > _nextLevelObstacleCount)
            {
                _createdObstacleCount = 0;

                if (_curLevel < _pools.Length)
                    _curLevel++;
            }

            var index = Mathf.Clamp(_curLevel, _startLevel, _pools.Length - 1);
            var track = Random.Range(ReferenceAggregator.WorldConstructor.FirstRoadLaneNumber,
                ReferenceAggregator.WorldConstructor.LastRoadLaneNumber + 1);
            var obstacle = _pools[index].RandomSpawn();
            ReferenceAggregator.WorldConstructor.ReplaceObject(obstacle, new CellPosition(track, 0 ,_lastPositionObstacle));
            _lastPositionObstacle += _distanceToObstacle + obstacle.DimensionByZ;
            _createdObstacles.Enqueue(obstacle);
            _curObstacleOnScene++;
        }

        /// <summary>
        /// Метод уничтожения препятствий.
        /// </summary>
        private void DisableObstacle()
        {
            var obstacle = _createdObstacles.Dequeue();
            obstacle.gameObject.SetActive(false);
            _curObstacleOnScene--;
        }

        #endregion
    }
}