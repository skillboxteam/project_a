﻿using System.Collections.Generic;
using System.Linq;
using Game.Common;
using Game.Common.Infrastructure;
using UnityEngine;

namespace Game.Obstacles.Management
{
    /// <summary>
    /// Пул объектов.
    /// </summary>
    public class ObstaclesPool : MonoBehaviour
    {
        #region Поля

        /// <summary>
        /// Объект для пула.
        /// </summary>
        [SerializeField]
        private ObstacleBase[] _objectPrefabs = null;

        /// <summary>
        /// Инициализированный размер пула.
        /// </summary>
        [SerializeField]
        private int _initSize = 0;

        /// <summary>
        /// Возможность к расширению пула.
        /// </summary>
        [SerializeField]
        private bool _ableToExpand = false;

        /// <summary>
        /// Создание объекта как дочернего.
        /// </summary>
        [SerializeField]
        private bool _spawnAsChild = false;

        /// <summary>
        /// Стэк объектов для пула.
        /// </summary>
        private Stack<ObstacleBase> _objects;

        #endregion

        #region Методы жизненного цикла

        /// <summary>
        /// Выполняет преинициализацию объекта.
        /// </summary>
        private void Awake()
        {
            _objects = new Stack<ObstacleBase>(_initSize);
        }

        /// <summary>
        /// Выполняет инициализацию объекта.
        /// </summary>
        private void Start()
        {
            for (var i = 0; i < _initSize; ++i)
            {
                var obj = CreateObject(_objectPrefabs[GetRandomIndex()]);
                obj.gameObject.SetActive(false);
            }
        }

        #endregion

        #region Остальные методы

        /// <summary>
        /// Метод ввода объекта в пул.
        /// </summary>
        /// <param name="obj">Объект для внесения в пул объектов.</param>
        public void PoolObject(ObstacleBase obj)
        {
            _objects.Push(obj);
        }

        /// <summary>
        /// Метод вывода объекта из пула.
        /// </summary>
        /// <returns>Объект из пула.</returns>
        public ObstacleBase Spawn()
        {
            ObstacleBase obj;

            if (!_objects.Any())
            {
                obj = _ableToExpand ? CreateObject(_objectPrefabs[GetRandomIndex()]) : null;
            }
            else
            {
                obj = _objects.Pop();
                obj.gameObject.SetActive(true);
            }

            return obj;
        }

        /// <summary>
        /// Метод создания объекта на сцене.
        /// </summary>
        /// <param name="prefab">Префаб создаваемого объекта.</param>
        /// <returns>Созданный объект.</returns>
        private ObstacleBase CreateObject(ObstacleBase prefab)
        {
            var result = _spawnAsChild ?
                ReferenceAggregator.WorldConstructor.Create(prefab, transform) :
                ReferenceAggregator.WorldConstructor.Create(prefab);
            return result;
        }

        /// <summary>
        /// Возвращает случайный индекс для выборки образца препятствия из массива.
        /// </summary>
        /// <returns>Индекс.</returns>
        private int GetRandomIndex()
        {
            return Random.Range(0, _objectPrefabs.Length);
        }

        #endregion
    }
}
