﻿using System.Linq;
using UnityEngine;

namespace Game.Obstacles.Management
{
    /// <summary>
    /// Группа пулов препятствий.
    /// </summary>
    public class ObstaclesPoolsGroup : MonoBehaviour
    {
        #region Поля

        /// <summary>
        /// Массив пулов.
        /// </summary>
        [SerializeField]
        private ObstaclesPool[] _pools = null;

        #endregion

        #region Методы

        /// <summary>
        /// Массив пулов.
        /// </summary>
        /// <param name="index">Индекс пула, из которого создается объект.</param>
        /// <returns>Объект из пула с индексом. Null, если нет пулов.</returns>
        public ObstacleBase Spawn(int index)
        {
            if (!_pools.Any())
                return null;

            return _pools.Length > index ? _pools[index].Spawn() : null;
        }

        /// <summary>
        /// Массив пулов.
        /// </summary>
        /// <returns>Объект из случайного пула. Null, если нет пулов.</returns>
        public ObstacleBase RandomSpawn()
        {
            if (!_pools.Any())
                return null;

            var poolIndex = Random.Range(0, _pools.Length);
            return _pools[poolIndex].Spawn();
        }

        #endregion
    }
}