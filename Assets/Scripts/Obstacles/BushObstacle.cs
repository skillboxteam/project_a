﻿using System;
using Game.Characters.Common;
using Game.Common;
using Game.Common.Infrastructure;
using Game.Effects;
using UnityEngine;

namespace Game.Obstacles
{
    /// <summary>
    /// Компонент для препятствий кустов.
    /// </summary>
    public class BushObstacle : ObstacleBase
    {
        /// <summary>
        /// Значение замедления при прохождении.
        /// </summary>
        [SerializeField, Range(0, 1)] private float _slowRatio = .5f;

        /// <summary>
        /// ссылка на фабрику эффектов.
        /// </summary>
        private EffectManager _effectManager;

        /// <summary>
        /// ссылка на текущий эффект.
        /// </summary>
        private BaseEffect _baseEffect;

        /// <summary>
        /// Действие, выполняемое при столкновении персонажа с препятствием.
        /// </summary>
        public override Action<Character> OnCollisionEnterAction =>
            character => _baseEffect = _effectManager.ApplySpeedTrigger(-_slowRatio * 100); // минус для дебафа, 100%

        /// <summary>
        /// Действие, выполняемое после выхода персонажа с зоны препятствия.
        /// </summary>
        public override Action<Character> OnCollisionExitAction => character => _baseEffect.CancelEffect();

        /// <summary>
        /// Выполняет инициализацию объекта.
        /// </summary>
        protected override void Start()
        {
            base.Start();
            _effectManager = ReferenceAggregator.EffectController;
        }
    }
}