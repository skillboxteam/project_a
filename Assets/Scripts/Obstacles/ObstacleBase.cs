﻿using System;
using Game.Characters.Common;
using Game.Common;
using Game.Obstacles.Management;
using UnityEngine;

namespace Game.Obstacles
{
    /// <summary>
    /// Базовый класс для препятствий.
    /// </summary>
    public abstract class ObstacleBase : GameObjectBase
    {
        #region Поля

        /// <summary>
        /// Поле пула объектов, к которому относится объект.
        /// </summary>
        private ObstaclesPool _pool;

        /// <summary>
        /// Урон, наносимый персонажу при столкновении.
        /// </summary>
        [SerializeField, Header("Настройка урона.")]
        private float _damage = 0;

        #endregion

        #region Свойства

        /// <summary>
        /// Действие, выполняемое при столкновении персонажа с препятствием.
        /// </summary>
        public abstract Action<Character> OnCollisionEnterAction { get; }

        /// <summary>
        /// Действие, выполняемое после выхода персонажа с зоны препятствия.
        /// </summary>
        public abstract Action<Character> OnCollisionExitAction { get; }

        /// <summary>
        /// Урон, наносимый персонажу при столкновении.
        /// </summary>
        protected float Damage => _damage;

        #endregion

        #region Методы жизненного цикла

        /// <summary>
        /// Метод преинициализации объекта.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            if (_pool == null)
                _pool = GetComponentInParent<ObstaclesPool>();
        }

        /// <summary>
        /// Метод инициализации объекта.
        /// </summary>
        protected override void Start()
        {
            base.Start();
        }

        /// <summary>
        /// Метод деактивация объекта.
        /// </summary>
        private void OnDisable()
        {
            if (_pool != null)
                _pool.PoolObject(this);
        }

        #endregion
    }
}