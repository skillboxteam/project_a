﻿using System;
using Game.Characters.Common;
using UnityEngine;

namespace Game.Obstacles
{
    /// <summary>
    /// Компонент для препятствий камней.
    /// </summary>
    public class SmallObstacle : ObstacleBase
    {
        [SerializeField] 
        private float _fallLength = 1.0f;
        
        /// <summary>
        /// Действие, выполняемое при столкновении персонажа с препятствием.
        /// </summary>
        public override Action<Character> OnCollisionEnterAction
        {
            get
            {
                return character =>
                {
                    character.StartFalling(DimensionByZ + _fallLength);
                    character.CurHealth -= Damage;
                };
            }
        }

        /// <summary>
        /// Действие, выполняемое после выхода персонажа с зоны препятствия.
        /// </summary>
        public override Action<Character> OnCollisionExitAction => null;
    }
}