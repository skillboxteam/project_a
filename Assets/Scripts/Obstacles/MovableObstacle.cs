﻿using System;
using Game.Characters.Common;
using Game.Environment.Cells.Description;
using UnityEngine;

namespace Game.Obstacles
{
    /// <summary>
    /// Класс для движущихся препятствий.
    /// </summary>
    public class MovableObstacle : ObstacleBase
    {
        /// <summary>
        /// Скорость передвижения.
        /// </summary>
        [SerializeField]
        private readonly float _moveSpeed;

        [SerializeField]
        private readonly CellPosition[] _movePoints;

        /// <summary>
        /// Действие, выполняемое при столкновении персонажа с препятствием.
        /// </summary>
        public override Action<Character> OnCollisionEnterAction => null;

        /// <summary>
        /// Действие, выполняемое после выхода персонажа с зоны препятствия.
        /// </summary>
        public override Action<Character> OnCollisionExitAction => null;
    }
}