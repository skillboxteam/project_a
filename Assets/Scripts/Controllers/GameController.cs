﻿using Game.Environment.Cells.Description;
using UnityEngine;

namespace Game.Controllers
{
    /// <summary>
    /// Контроллер игры.
    /// </summary>
    public class GameController : MonoBehaviour
    {
        #region Поля

        /// <summary>
        /// Начальная позиция игрока.
        /// </summary>
        [SerializeField]
        private CellPosition _startPlayerPos = CellPosition.Zero;

        /// <summary>
        /// Начальная позиция колобка.
        /// </summary>
        [SerializeField]
        private CellPosition _startKolobokPos = CellPosition.Zero;

        /// <summary>
        /// Пометка для упрощения тестирования игры во время разработки.
        /// </summary>
        [SerializeField]
        private bool _isDebugMode = true;
        
        #endregion

        #region Константы

        /// <summary>
        /// Индекс слоя препятствий.
        /// </summary>
        public const int ObstaclesLayerIndex = 9;

        #endregion

        #region Свойства

        /// <summary>
        /// Начальная позиция игрока.
        /// </summary>
        public CellPosition StartPlayerPos => _startPlayerPos;

        /// <summary>
        /// Начальная позиция колобка.
        /// </summary>
        public CellPosition StartKolobokPos => _startKolobokPos;

        /// <summary>
        /// Пометка для упрощения тестирования игры во время разработки.
        /// </summary>
        public bool IsDebugMode => _isDebugMode;

        #endregion

        #region Методы жизненного цикла

        #endregion
    }
}
