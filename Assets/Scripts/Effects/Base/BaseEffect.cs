﻿using System;
using Game.Characters.Common;
using UnityEngine;

namespace Game.Effects
{
    /// <summary>
    /// Базовый компонент для эффектов.
    /// </summary>
    public class BaseEffect : MonoBehaviour
    {
        #region Свойства
        
        /// <summary>
        /// Алгоритм отмены эффекта.
        /// </summary>
        public IEffectBehaviour EffectBehavior { get; set; }

        /// <summary>
        /// Объект на который будем влиять.
        /// </summary>
        protected Character Character { get; set; }

        /// <summary>
        /// Родительский объект для всех эффектов.
        /// </summary>
        protected Transform EffectRoot { get; set; }

        /// <summary>
        /// Эффект применился.
        /// </summary>
        protected Action<float> AppliedEvent { get; set; }
        
        #endregion

        #region Остальные методы
        /// <summary>
        /// Эффект применился.
        /// </summary>
        /// <param name="delta">дельта</param>
        protected void AppliedOn(float delta)
        {
            AppliedEvent?.Invoke(delta);
        }

        /// <summary>
        /// Отменить эффект.
        /// </summary>
        public virtual void CancelEffect()
        {
            print("Не переопределен метод CancelEffect");
        }

        /// <summary>
        /// Применить эффект.
        /// </summary>
        protected virtual void ApplyEffect()
        {
            print("Не переопределен метод Apply");
        }
        #endregion

        #region Методы жизненного цикла

        /// <summary>
        /// Метод инициализации объекта.
        /// </summary>
        protected virtual void Start()
        {
            Character = GetComponentInParent<Character>();
            EffectBehavior = GetComponent<IEffectBehaviour>();
            EffectRoot = Character.FindChild("EffectRoot");            
            EffectBehavior.SetCancelEffect(Character, CancelEffect);
        }
        #endregion
    }
}