﻿namespace Game.Effects
{
    /// <summary>
    /// Список всех доступных префабов для загрузки в менеджер.
    /// </summary>
    public enum Effects
    {
        SpeedContinuousEffect = 0,
        SpeedTriggerEffect,
    }
}
