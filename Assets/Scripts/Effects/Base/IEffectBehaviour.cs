﻿using System;
using Game.Characters.Common;

/// <summary>
/// Поведение эффекта
/// </summary>
public interface IEffectBehaviour
{
    /// <summary>
    /// Стратегия отмены эффекта.
    /// </summary>
    /// <param name="character">Объект воздействия.</param>
    /// <param name="effectExpired">Что произойдет при отмене эффекта.</param>
    void SetCancelEffect(Character character, Action effectExpired);
}
