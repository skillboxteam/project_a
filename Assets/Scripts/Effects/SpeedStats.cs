﻿namespace Game.Effects
{
    /// <summary>
    /// Характеристики скорости.
    /// </summary>
    public class SpeedStats
    {
        /// <summary>
        /// Скорость перещения без модификаторов.
        /// </summary>
        private float _normalMoveSpeed = 0;

        /// <summary>
        /// Текущая скорость перещения.
        /// </summary>
        private float _curMoveSpeed = 0;

        /// <summary>
        /// Макс. скорость перемещения.
        /// </summary>
        private float _maxMoveSpeed = 0;

        /// <summary>
        /// Минимальная скорость перемещения.
        /// </summary>
        private float _minMoveSpeed = 0;

        /// <summary>
        /// Скорость передвижения без модификаторов.
        /// </summary>
        public float NormalMoveSpeed { get => this._normalMoveSpeed; set => this._normalMoveSpeed = value; }

        /// <summary>
        /// Текущая скорость перемещения.
        /// </summary>
        public float CurMoveSpeed { get => this._curMoveSpeed; set => this._curMoveSpeed = value; }

        /// <summary>
        /// Максимально возможная скорость перемещения.
        /// </summary>
        public float MaxMoveSpeed { get => this._maxMoveSpeed; set => this._maxMoveSpeed = value; }

        /// <summary>
        /// Минимально возможная скорость перемещения.
        /// </summary>
        public float MinMoveSpeed { get => this._minMoveSpeed; set => this._minMoveSpeed = value; }
    }
}