﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Effects
{
    /// <summary>
    /// Контроллер эффектов
    /// </summary>
    public class EffectManager : MonoBehaviour
    {
        #region Поля
        /// <summary>
        /// Список префабов с эффектами.
        /// </summary>
        [SerializeField]
        private List<GameObject> _effectsPrefab = null;
        #endregion

        #region Остальные методы

        /// <summary>
        /// Наложить эффект скорости c отменой по таймеру.
        /// </summary>
        /// <param name="target">Родительский объект</param>
        /// <param name="factor">Значение модификатора для скорости</param>
        /// <param name="duration">Время до отмены</param>
        /// <returns>Эффект</returns>
        public BaseEffect ApplySpeedContinuous(GameObject target, float factor, float duration)
        {
            var effect = CreateEffect(Effects.SpeedContinuousEffect);
            effect.transform.SetParent(target.transform);
            effect.GetComponent<SpeedEffect>().FactorSpeed = factor;
            effect.GetComponent<ContinuousBehaviour>().Duration = duration;
            return effect.GetComponent<SpeedEffect>();
        }

        /// <summary>
        /// Наложить эффект скорости c таймером.
        /// </summary>
        /// <param name="factor">Значение модификатора для скорости</param>
        /// <param name="duration">Время до отмены</param>
        /// <returns>Эффект</returns>
        public BaseEffect ApplySpeedContinuous(float factor, float duration)
        {
            return ApplySpeedContinuous(gameObject, factor, duration);
        }

        /// <summary>
        /// Наложить эффект скорости с отменой по событию.
        /// </summary>
        /// <param name="target">Цель, куда цеплять эффект.</param>
        /// <param name="factor">Значение модификатора для скорости.</param>
        /// <returns>Эффект</returns>
        public BaseEffect ApplySpeedTrigger(GameObject target, float factor)
        {
            var effect = CreateEffect(Effects.SpeedTriggerEffect);
            effect.transform.SetParent(target.transform);
            effect.GetComponent<SpeedEffect>().FactorSpeed = factor;
            return effect.GetComponent<SpeedEffect>();
        }

        /// <summary>
        /// Наложить эффект скорости с отменой по событию.
        /// </summary>
        /// <param name="factor">Значение модификатора для скорости.</param>
        /// <returns>Эффект</returns>
        public BaseEffect ApplySpeedTrigger(float factor)
        {
            return ApplySpeedTrigger(gameObject, factor);
        }

        /// <summary>
        /// Создает новый объект из перечисления префабов.
        /// </summary>
        /// <param name="numEffect">Эффект для применения</param>
        /// <returns>новый объект эффекта</returns>
        private GameObject CreateEffect(Effects numEffect)
        {
            var speedPrefab = _effectsPrefab[(int)numEffect];
            return GameObject.Instantiate<GameObject>(speedPrefab);
        }

        /// <summary>
        /// Заполнение массива эффектов из ресурсов.
        /// </summary>
        private void LoadEffectPrefabs()
        {
            var path = @"Prefabs/Effects/";

            var effectsNames = System.Enum.GetNames(typeof(Effects));
            _effectsPrefab = new List<GameObject>();
            foreach (var name in effectsNames)
            {
                var effect = Resources.Load<GameObject>(path + name);
                _effectsPrefab.Add(effect);
            }
        }
        #endregion

        #region Методы жизненного цикла
        /// <summary>
        /// Метод инициализации объекта.
        /// </summary>
        private void Awake()
        {
            LoadEffectPrefabs();
        }
        #endregion
    }
}