﻿using Game.Characters.Common;
using Game.Common.Infrastructure;
using UnityEngine;

namespace Game.Effects
{
    /// <summary>
    /// Тест эффектов по нажатию клавиш
    /// </summary>
    public class ExampleApplyEffects : MonoBehaviour
    {
        /// <summary>
        /// Множитель скорости.
        /// </summary>
        [SerializeField]
        private float _factor = 30;

        /// <summary>
        /// Время до окончания эффекта.
        /// </summary>
        [SerializeField]
        private float _duration = 3.0f;

        /// <summary>
        /// Куда цеплять эффект.
        /// </summary>
        [SerializeField]
        private GameObject _effectRoot = null;

        /// <summary>
        /// Менеджер эффектов
        /// </summary>
        private EffectManager _effectManager;

        /// <summary>
        /// объект для воздействия
        /// </summary>
        [SerializeField]
        private Character _character = null;
        
        private void Start()
        {
            _effectManager = ReferenceAggregator.EffectController;
        }
        
        private void Update()
        {
            // Добавить эффект скорости по таймеру +
            if (Input.GetKeyDown(KeyCode.Insert))
            {
                _effectManager.ApplySpeedContinuous(_effectRoot, _factor, _duration);
            }

            // Добавить эффект скорости по таймеру -
            if (Input.GetKeyDown(KeyCode.Delete))
            {
                _effectManager.ApplySpeedContinuous(_effectRoot, -_factor, _duration);
            }

            // Добавить эффект скорости по событию +
            if (Input.GetKeyDown(KeyCode.Home))
            {
                // effectMeneger.ApplySpeedContinuous(_effectRoot, factor, duration);
                _effectManager.ApplySpeedTrigger(_effectRoot, _factor);
            }

            // Добавить эффект скорости по событию -
            if (Input.GetKeyDown(KeyCode.End))
            {
                // effectMeneger.ApplySpeedContinuous(_effectRoot, -factor, duration);
                _effectManager.ApplySpeedTrigger(_effectRoot, -_factor);
            }

            // Активировать событие отмены эффекта
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _character.OnEffectCanceled?.Invoke();
            }
        }
    }
}