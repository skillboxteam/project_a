﻿using System;

namespace Game.Effects
{
    /// <summary>
    /// Модуль для расчета скорости.
    /// </summary>
    public class SpeedCalculator
    {
        /// <summary>
        /// Данные о передвижении.
        /// </summary>
        private SpeedStats _moveStats;

        /// <summary>   
        /// Initializes a new instance of the SpeedCalculator class.
        /// </summary>
        public SpeedCalculator()
        {
        }

        /// <summary>   
        /// Initializes a new instance of the SpeedCalculator class.
        /// Инициализирует SpeedCalculator данными о передвижении.
        /// </summary>
        /// <param name="stats">параметры передвижения</param>
        public SpeedCalculator(SpeedStats stats)
        {
            this._moveStats = stats;
        }

        /// <summary>
        /// Расчет дельты для изменения скорости
        /// </summary>
        /// <param name="factorSpeed">Множитель скорости</param>
        /// <returns>Расчитаную дельту</returns>
        public float CalculateDelta(float factorSpeed)
        {
            var valueForPercent = 100;
            return this._moveStats.NormalMoveSpeed * (factorSpeed / valueForPercent);
        }

        /// <summary>
        /// Будет ли меняться скорость с текущей дельтой.
        /// Нет смысла применять новый эффект если мы уже находимся на граничных
        /// условиях и дельта выводит скорость дальше за границы.
        /// </summary>
        /// <param name="deltaSpeed">Дельта скорости</param>
        /// <returns>Флаг, изменится ли скорость</returns>
        public bool SpeedMayChange(float deltaSpeed)
        {
            var minMoveSpeed = this._moveStats.MinMoveSpeed;
            var maxMoveSpeed = this._moveStats.MaxMoveSpeed;
            var curMoveSpeed = this._moveStats.CurMoveSpeed;

            var speedIncrease = deltaSpeed > 0;    // Скорость возрастает
            var speedDecrease = deltaSpeed < 0;    // Скорость уменьшается

            var speedMinimum = curMoveSpeed == minMoveSpeed;        // Скорость минимальна
            var speedMaximum = curMoveSpeed == maxMoveSpeed;        // Скорость максимальна

            var speedLessMax = curMoveSpeed < maxMoveSpeed;         // Скорость меньше максимальной
            var speedGreaterMin = curMoveSpeed > minMoveSpeed;      // Скорость больше минимальной

            var speedAcceptable = speedGreaterMin && speedLessMax;  // Скорость в пределах допустимого
            if (speedAcceptable)
                return speedAcceptable;

            var speedMayIncrease = speedMinimum && speedIncrease;   // Скорость может повыситься
            var speedMayDecrease = speedMaximum && speedDecrease;   // Скорость может снизиться

            return speedMayIncrease || speedMayDecrease;
        }

        /// <summary>
        /// Нормализирует дельту если она близка к граничным скоростям.
        /// <![CDATA[
        /// Если скорость после применения дельты будет меньше минимальной, тогда:
        /// delta = ???;
        /// min = 5;
        /// delta = -2;
        /// speed = 6;
        /// resultSpeed = 4 = (delta = -2) + (speed = 6)
        /// if  (resultSpeed = 4) < (min = 5)
        ///     delta = 1 = (speed = 6) - (min = 5)
        /// ************************************************************************
        /// delta = -3;
        /// speed = 6;
        /// resultSpeed = 3 = (delta = -3) + (speed = 6)
        /// if  (resultSpeed = 3) < (min = 5)
        ///     delta = 1 =  (speed = 6) - (min = 5)
        /// ////////////////////////////////////////////////////////////////////////
        /// Если скорость после применения дельты будет больше максимальной, тогда:
        /// max = 30;
        /// delta = 6;
        /// speed = 25;
        /// delta = ???;
        /// resultSpeed = 31 = (delta = 6) + (speed = 25)
        /// if  (resultSpeed = 31) > (max = 30)
        ///     delta = 5 =  ( max = 30) - (speed = 25)
        /// ]]>
        /// </summary>
        /// <param name="deltaSpeed">Дельта скорости</param>
        /// <returns>Обработаная дельта</returns>
        public float NormalizeDelta(float deltaSpeed)
        {
            var resultDelta = deltaSpeed;
            var deltaSign = Math.Sign(deltaSpeed);
            var resultSpeed = deltaSpeed + this._moveStats.CurMoveSpeed;

            if (resultSpeed < this._moveStats.MinMoveSpeed)
            {
                resultDelta = (this._moveStats.CurMoveSpeed - this._moveStats.MinMoveSpeed) * deltaSign;
            }
            else
            if (resultSpeed > this._moveStats.MaxMoveSpeed)
            {
                resultDelta = (this._moveStats.MaxMoveSpeed - this._moveStats.CurMoveSpeed) * deltaSign;
            }

            return resultDelta;
        }
    }
}
