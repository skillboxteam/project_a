﻿using UnityEngine;

namespace Game.Effects
{
    /// <summary>
    /// Эффект для изменения скорости передвижения.
    /// </summary>
    public class SpeedEffect : BaseEffect
    {
        #region Поля
        
        /// <summary>
        /// Калькулятор скорости для эффекта.
        /// </summary>
        private SpeedCalculator _calculator;

        /// <summary>
        /// дельта скорости перемещения
        /// </summary>
        private float _deltaSpeed;

        /// <summary>
        /// Коэффициент скорости после эффекта.
        /// </summary>
        [SerializeField]
        private float _factorSpeed;
        #endregion

        #region Свойства
        
        /// <summary>
        /// Коэффициент скорости после эффекта.
        /// </summary>
        public float FactorSpeed { get => _factorSpeed; set => _factorSpeed = value; }
        
        #endregion

        #region Методы
        
        /// <summary>
        /// Применить эффект.
        /// </summary>
        protected override void ApplyEffect()
        {
            _deltaSpeed = _calculator.CalculateDelta(FactorSpeed);
            
            if (!_calculator.SpeedMayChange(_deltaSpeed))
            {
                Destroy(gameObject);
                return;
            }

            _deltaSpeed = _calculator.NormalizeDelta(_deltaSpeed);
            AppliedOn(_deltaSpeed);
        }

        /// <summary>
        /// Отменить эффект.
        /// </summary>
        public override void CancelEffect()
        {
            AppliedOn(-_deltaSpeed);
            
            if (gameObject != null)
                Destroy(gameObject);
        }

        private void AddSpeedToCharacter(float deltaSpeed)
        {
            Character.CurMoveSpeed += deltaSpeed;
        }
        
        #endregion

        #region Методы жизненного цикла
        
        /// <summary>
        /// Метод инициализации объекта.
        /// </summary>
        protected override void Start()
        {
            base.Start();

            var stats = new SpeedStats()
            {
                NormalMoveSpeed = Character.NormalMoveSpeed,
                MaxMoveSpeed = Character.MaxMoveSpeed,
                MinMoveSpeed = Character.MinMoveSpeed,
                CurMoveSpeed = Character.CurMoveSpeed,
            };
            _calculator = new SpeedCalculator(stats);

            AppliedEvent += AddSpeedToCharacter;
            ApplyEffect();
        }

        /// <summary>
        /// Выполняется перед разрушением объекта.
        /// </summary>
        private void OnDestroy()
        {
            if(AppliedEvent != null)
                AppliedEvent -= AddSpeedToCharacter;
        }
        
        #endregion
    }
}
