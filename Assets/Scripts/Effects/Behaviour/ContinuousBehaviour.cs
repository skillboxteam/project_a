﻿using System;
using Game.Characters.Common;
using UnityEngine;

namespace Game.Effects
{
    /// <summary>
    /// Временные эффекты, спадающие по окончанию времени.
    /// </summary>
    [RequireComponent(typeof(Timer))]
    public class ContinuousBehaviour : MonoBehaviour, IEffectBehaviour
    {
        /// <summary>
        /// Компонент отчитывающий время.
        /// </summary>
        private Timer _timer = null;

        /// <summary>
        /// Действие происходящие по окончанию таймера.
        /// </summary>
        private Action _effectExpired;

        /// <summary>
        /// Время влияния эффекта до его рассеивания.
        /// </summary>
        [SerializeField]
        private float _duration;

        /// <summary>
        /// Время влияния эффекта до его рассеивания.
        /// </summary>
        public float Duration { get => _duration; set => _duration = value; }

        /// <summary>
        /// Начать отсчет до рассеивания эффекта.
        /// </summary>
        /// <param name="character">Объект воздействия.</param>
        /// <param name="effectExpired">Что произойдет при отмене эффекта.</param>
        public void SetCancelEffect(Character character, Action effectExpired)
        {
            _timer = GetComponent<Timer>();
            _effectExpired += effectExpired;

            _timer.TimerExpired += _effectExpired;
            _timer.StarTimer(_duration);
        }

        /// <summary>
        /// Выполняется перед уничтожением
        /// </summary>
        private void OnDestroy()
        {
            _timer.TimerExpired -= _effectExpired;
        }
    }
}