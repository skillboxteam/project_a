﻿using System;
using Game.Characters.Common;
using UnityEngine;

namespace Game.Effects
{
    /// <summary>
    /// Jтменf эффекта по событию отмены на персонаже.
    /// </summary>
    public class TriggeredBehaviour : MonoBehaviour, IEffectBehaviour
    {
        /// <summary>
        /// Объект создающий событие.
        /// </summary>
        private Character _character;

        /// <summary>
        /// Действие происходящие по событию.
        /// </summary>
        private Action _effectExpired;

        /// <summary>        
        /// Настроить на событие EffectCanceled в MovableObject.
        /// </summary>
        /// <param name="character">Объект воздействия.</param>
        /// <param name="effectExpired">Что произойдет при отмене эффекта.</param>
        public void SetCancelEffect(Character character, Action effectExpired)
        {
            _character = character;
            _effectExpired += effectExpired;
            character.OnEffectCanceled += _effectExpired;
        }

        /// <summary>
        /// Выполняется при уничтожении объекта.
        /// </summary>
        private void OnDestroy()
        {
            if (_character?.OnEffectCanceled != null)
                _character.OnEffectCanceled -= _effectExpired;
        }
    }
}