﻿using Game.Common;
using UnityEngine;

namespace Game.Environment.Abstractions
{
    /// <summary>
    /// Фабрика игровых объектов.
    /// </summary>
    public interface IGameObjectFabric
    {
        /// <summary>
        /// Создает игровой объект на сцене.
        /// </summary>
        /// <typeparam name="T">MonoBehaviour.</typeparam>
        /// <param name="source">Образец объекта.</param>
        /// <param name="parent">Родительский объект.</param>
        /// <returns>Ссылку на созданный объект.</returns>
        T CreateObject<T>(T source, Transform parent = null) where T : MonoBehaviour;

        /// <summary>
        /// Перемещает объект на сцене.
        /// </summary>
        /// <param name="obj">Ссылка на объект.</param>
        /// <param name="position">Новая позиция.</param>
        void ReplaceObject(MonoBehaviour obj, Vector3 position);
    }
}
