using System.Collections.Generic;
using Game.Common;
using Game.Environment.Cells.Description;

namespace Game.Environment.Abstractions
{
    /// <summary>
    /// Фабрика элементов окружения.
    /// </summary>
    public interface IEnvironmentFabric
    {
        GameObjectBase CreatePart(GameObjectBase prefab, CellPosition position);
    
        /// <summary>
        /// Создает случайный участок окружения.
        /// </summary>
        /// <param name="position">Позиция нового участка.</param>
        /// <returns>Ссылка на созданный участок.</returns>
        GameObjectBase CreateRandomPart(CellPosition position);

        /// <summary>
        /// Создает последовательность из случайных участков окружения.
        /// </summary>
        /// <param name="firstPos">Позиция перевого участка.</param>
        /// <param name="count">Длина последовательности.</param>
        /// <returns>Список ссылок на созданные объекты сцены.</returns>
        ICollection<GameObjectBase> CreatePartsInRandomSequence(CellPosition firstPos, int count);

        /// <summary>
        /// Создает последоватлеьность из указаны участков окружения.
        /// </summary>
        /// <param name="firstPos">Позиция перевого участка.</param>
        /// <param name="parts">Образцы участков для создания на сцене.</param>
        /// <returns>Список ссылок на созданные объекты сцены.</returns>
        ICollection<GameObjectBase> CreateParts(CellPosition firstPos, params GameObjectBase[] parts);
    }
}