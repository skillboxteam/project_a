﻿using Game.Environment.Cells;
using Game.Environment.Cells.Description;
using System.Collections.Generic;
using Game.Common;
using UnityEngine;

namespace Game.Environment.Abstractions
{
    /// <summary>
    /// Фабрика клеток.
    /// </summary>
    public interface ICellsFabric
    {
        /// <summary>
        /// Создает пустую клетку.
        /// </summary>
        /// <param name="worldPosition">Позиция клетки в мировой системе.</param>
        /// <param name="cellPosition">Позиция клетки в клеточной системе.</param>
        /// <returns>Ссылку на созданную клетку.</returns>
        Cell CreateEmptyCell(Vector3 worldPosition, CellPosition cellPosition);

        /// <summary>
        /// Заполняет мир пустыми клетками.
        /// </summary>
        /// <param name="firstCellWorldPosition">Позиция первой клетки в мировой системе.</param>
        /// <param name="cellsCountOnX">Количество клеток по оси X.</param>
        /// <param name="cellsCountOnY">Количество клеток по оси Y.</param>
        /// <param name="cellsCountOnZ">Количество клеток по оси Z.</param>
        /// <returns>Список ссылок на объекты клеток на сцене.</returns>
        ICollection<Cell> FillWorld(Vector3 firstCellWorldPosition, int cellsCountOnX, int cellsCountOnY, int cellsCountOnZ);

        /// <summary>
        /// Возвращает клетку по координатам.
        /// </summary>
        /// <param name="position">Координаты искомой клетки.</param>
        /// <returns>Ссылка на объект клетки.</returns>
        Cell GetCell(CellPosition position);

        /// <summary>
        /// Возвращает список клеток по списку координат.
        /// </summary>
        /// <param name="positions">Список искомых координат.</param>
        /// <returns>Список ссылок на клетки.</returns>
        IEnumerable<Cell> GetCells(IEnumerable<CellPosition> positions);

        Vector3 GetWorldPositionByCellPosition(CellPosition position);

        IEnumerable<Cell> GetCellsOnRowWithType(int rowNumber, CellType type);

        void SetFillToCells(GameObjectBase obj);

        void SetFillToCell(CellDescription cellDescription, GameObjectBase obj);

        void SetEmptyToCells(IEnumerable<CellPosition> cellPositions);

        void SetEmptyToCell(CellPosition cellPosition);
    }
}
