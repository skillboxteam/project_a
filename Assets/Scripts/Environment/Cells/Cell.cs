﻿using System.Collections.Generic;
using System.Linq;
using Game.Common;
using Game.Environment.Cells.Description;
using Game.ForEditor;
using UnityEngine;

namespace Game.Environment.Cells
{
    public class Cell : MonoBehaviour
    {
        #region Поля

        [SerializeField, ReadOnly]
        private CellDescription _description;

        #endregion

        #region Свойства

        public Vector3 Position => transform.position;

        public Vector3 Dimensions => transform.localScale;

        public Transform[] Children => transform.Cast<Transform>().ToArray();

        public CellDescription Description
        {
            get => _description;
            set => _description = value;
        }

        public CellPosition CellPosition => Description.CellPosition;

        public CellType Type
        {
            get => Description.Type;
            set => _description.Type = value;
        }
        
        public GameObjectBase Object { get; set; }

        public bool HasObject => Object != null;

        #endregion

        #region Методы

        public void Initialize(CellPosition cellPosition, CellType type, GameObjectBase obj = null)
        {
            Description = new CellDescription { CellPosition = cellPosition, Type = type };
            Object = obj;
            name = $"Cell {Description.CellPosition} {Description.Type}";
        }

        #endregion
    }
}
