﻿using System;
using UnityEngine;

namespace Game.Environment.Cells.Description
{
    /// <summary>
    /// Описание клетки.
    /// </summary>
    [Serializable]
    public struct CellDescription
    {
        /// <summary>
        /// Позиция в клеточной системе координат.
        /// </summary>
        [SerializeField]
        private CellPosition _cellPosition;

        /// <summary>
        /// Тип клетки.
        /// </summary>
        [SerializeField]
        private CellType _type;

        /// <summary>
        /// Позиция в клеточной системе координат.
        /// </summary>
        public CellPosition CellPosition
        {
            get => _cellPosition;
            set => _cellPosition = value;
        }

        /// <summary>
        /// Тип клетки.
        /// </summary>
        public CellType Type
        {
            get => _type;
            set => _type = value;
        }

        public CellDescription(CellPosition cellPosition, CellType type)
        {
            _cellPosition = cellPosition;
            _type = type;
        }
    }
}
