﻿namespace Game.Environment.Cells.Description
{
    /// <summary>
    /// Типы клеток.
    /// </summary>
    public enum CellType
    {
        CanRun, // Можно пробежать (клетка свободна).
        CanRunButNotRecommend, // Можно пробежать, но не рекомендуется (мягкое препятствие).
        CanSlide, // Можно проскользить (препятствие на высоте).
        CantRunAndSlide, // Нельзя пробежать или проскользить (твердое препятствие).
        NotDefined // Не определено (будет проходить движущееся препятствие).
    }
}