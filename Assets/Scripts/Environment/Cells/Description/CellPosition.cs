﻿using System;
using UnityEngine;

namespace Game.Environment.Cells.Description
{
    /// <summary>
    /// Описание позиции в клеточной системе координат.
    /// </summary>
    [Serializable]
    public struct CellPosition
    {
        #region Поля

        /// <summary>
        /// Комер клетки по оси X.
        /// </summary>
        [Header("Позиция объекта")]
        [SerializeField]
        private float _x;

        /// <summary>
        /// Номер клетки по оси Y.
        /// </summary>
        [SerializeField]
        private float _y;
        
        /// <summary>
        /// Номер клетки по оси Z.
        /// </summary>
        [SerializeField]
        private float _z;

        #endregion

        #region Свойства

        /// <summary>
        /// Номер клетки по оси X.
        /// </summary>
        public float X => _x;

        /// <summary>
        /// Номер клетки по оси Y.
        /// </summary>
        public float Y => _y;

        /// <summary>
        /// Номер клетки по оси Z.
        /// </summary>
        public float Z => _z;

        public CellPosition Forward => new CellPosition(0, 0, 1);
        
        public static CellPosition Zero => new CellPosition(0, 0 , 0);
        
        #endregion

        #region Конструктор

        /// <summary>
        /// Инициализирует объект.
        /// </summary>
        /// <param name="x">Номер клетки по оси X.</param>
        /// <param name="z">Номер клетки по оси Z.</param>
        /// <param name="y">Номер клетки по оси Y.</param>
        public CellPosition(float x, float y, float z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Возвращает строковое прдставление объекта.
        /// </summary>
        /// <returns>Строковое прдставление.</returns>
        public override string ToString()
        {
            return $"X:{_x} Y:{_y} Z:{_z}";
        }

        /// <summary>
        /// Сравнивает объект с другим.
        /// </summary>
        /// <param name="obj">Другой объект.</param>
        /// <returns>Возвращает true, если свойства X и Z обоих объектов равны, иначе false.
        /// Возвращет false, если тип объекта obj отличается от типа текущего объекта.</returns>
        public override bool Equals(object obj)
        {
            if (obj is CellPosition other)
                return X == other.X &&
                    Z == other.Z &&
                    Y == other.Y;

            return false;
        }

        /// <summary>
        /// Возвращает хэш-код объекта.
        /// </summary>
        /// <returns>Хэш-код.</returns>
        /// <remarks>Возвращает результат Object.GetHashCode.</remarks>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public Vector3 ToVector3()
        {
            return new Vector3(_x, _y, _z);
        }

        public CellPosition Ceil()
        {
            return new CellPosition(Mathf.Ceil(_x), Mathf.Ceil(_y), Mathf.Ceil(_z));
        }

        public CellPosition Truncate()
        {
            return new CellPosition((int)_x, (int)_y, (int)_z);
        }
        
        public float Magnitude()
        {
            return Mathf.Sqrt(_x * _x + _y * _y + _z * _z);
        }
        
        #endregion

        #region Операторы

        /// <summary>
        /// Перегрузка операции умножения на float.
        /// </summary>
        /// <param name="pos">Первый множитель.</param>
        /// <param name="multi">Второй множитель.</param>
        /// <returns>Вектор X, Y, Z которого умножены на multi.</returns>
        public static CellPosition operator *(CellPosition pos, float multi)
        {
            return new CellPosition(pos.X * multi, pos.Y * multi, pos.Z * multi);
        }

        /// <summary>
        /// Перегрузка операции сложения векторов.
        /// </summary>
        /// <param name="pos1">Первое слагаемое.</param>
        /// <param name="pos2">Второе слагаемое.</param>
        /// <returns>Вектор X, Z которого получены путем их сложения.</returns>
        public static CellPosition operator +(CellPosition pos1, CellPosition pos2)
        {
            return new CellPosition(pos1.X + pos2.X, pos1.Y + pos2.Y, pos1.Z + pos2.Z);
        }

        /// <summary>
        /// Перегрузка операции вычитания векторов.
        /// </summary>
        /// <param name="pos1">Уменьшаемое.</param>
        /// <param name="pos2">Вычитаемое.</param>
        /// <returns>Вектор X, Z которого получены путем вычисления их разницы.</returns>
        public static CellPosition operator -(CellPosition pos1, CellPosition pos2)
        {
            return new CellPosition(pos1.X - pos2.X, pos1.Y - pos2.Y, pos1.Z - pos2.Z);
        }

        /// <summary>
        /// Перегрузка операции равенства.
        /// </summary>
        /// <param name="pos1">Левый вектор сравнения.</param>
        /// <param name="pos2">Равый вектор сравнения.</param>
        /// <returns>Возвращает true, если длина левого вектора равна длине правого, иначе false.</returns>
        public static bool operator ==(CellPosition pos1, CellPosition pos2)
        {
            return pos1.Magnitude() == pos2.Magnitude();
        }

        /// <summary>
        /// Перегрузка операции неравенства.
        /// </summary>
        /// <param name="pos1">Левый вектор сравнения.</param>
        /// <param name="pos2">Равый вектор сравнения.</param>
        /// <returns>Возвращает true, если длина левого вектора не равна длине правого, иначе false.</returns>
        public static bool operator !=(CellPosition pos1, CellPosition pos2)
        {
            return !pos1.Equals(pos2);
        }

        public static bool operator >(CellPosition pos1, CellPosition pos2)
        {
            return pos1.Magnitude() > pos2.Magnitude();
        }
        
        public static bool operator <(CellPosition pos1, CellPosition pos2)
        {
            return pos1.Magnitude() < pos2.Magnitude();
        }
        
        #endregion
    }
}