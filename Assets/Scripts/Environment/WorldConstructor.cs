﻿using Game.Common;
using Game.Environment.Abstractions;
using Game.Environment.Cells;
using Game.Environment.Cells.Description;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Game.Characters.Common;
using Game.Common.Exceptions;
using Game.Common.Infrastructure;
using UnityEngine;

namespace Game.Environment
{
    /// <summary>
    /// Конструктор игровой сцены.
    /// </summary>
    public class WorldConstructor : MonoBehaviour
    {
        #region Зависимости

        /// <summary>
        /// Фабрика игровых объектов.
        /// </summary>
        private IGameObjectFabric _gameObjectFabric;

        /// <summary>
        /// Фабрика клеток.
        /// </summary>
        private ICellsFabric _cellsFabric;

        private IEnvironmentFabric _environmentFabric;

        public static ICellsFabric CellsFabric => _instance._cellsFabric;
        
        #endregion

        #region Поля

        /// <summary>
        /// Количество создаваемых клеток по оси X.
        /// </summary>
        [SerializeField, Header("Настройка клеточной системы")]
        private int _cellsCountOnX = 10;

        /// <summary>
        /// Количество создаваемых клеток по оси Y.
        /// </summary>
        [SerializeField]
        private int _cellsCountOnY = 100;

        /// <summary>
        /// Количество создаваемых клеток по оси Z.
        /// </summary>
        [SerializeField]
        private int _cellsCountOnZ = 100;

        [SerializeField]
        private Vector3 _startCellPosition = Vector3.zero;

        [SerializeField]
        private Cell _cellPrefab = null;

        [SerializeField]
        private Transform _cellParent = null;

        /// <summary>
        /// Пометка, требуется ли отображать клетки на сцене.
        /// </summary>
        [SerializeField]
        private bool _isShowCellsOnScene = false;
        
        /// <summary>
        /// Значение координаты самой левой полосы по оси X.
        /// </summary>
        [SerializeField, Header("Настройка игровой дорожки")]
        private int _firstRoadLaneNumber = 0;
        
        /// <summary>
        /// Значение координаты самой правой полосы по оси X.
        /// </summary>
        [SerializeField]
        private int _lastRoadLaneNumber = 3;
        
        /// <summary>
        /// Дистанция до уничтожения объектов после их прохождения.
        /// </summary>
        [SerializeField, Header("Настройка появления/уничтожения объектов")]
        private float _distanceToRemoveObjects = 5.0f;

        [SerializeField, Header("Настройка окружения")]
        private GameObjectBase[] _envPartsPrefabs = null;

        [SerializeField]
        private CellPosition _startEnvPosition = CellPosition.Zero;

        [SerializeField] 
        private int _countEnvParts = 10;

        [SerializeField] 
        private Transform _envParent = null;

        private static WorldConstructor _instance;
        
        #endregion

        #region Свойства

        /// <summary>
        /// Значение координаты самой левой полосы по оси X.
        /// </summary>
        public int FirstRoadLaneNumber => _firstRoadLaneNumber;

        /// <summary>
        /// Значение координаты самой правой полосы по оси X.
        /// </summary>
        public int LastRoadLaneNumber => _lastRoadLaneNumber;

        #endregion
        
        #region Методы жизненного цикла

        private void Awake()
        {
            _instance = this;
            _gameObjectFabric = new GameObjectFabric();
            _cellsFabric = new CellsFabric(_gameObjectFabric, _cellPrefab, _cellParent, _isShowCellsOnScene);
            _environmentFabric = new EnvironmentFabric(_gameObjectFabric, _cellsFabric, _envPartsPrefabs, _envParent);
        }
        
        private void Start()
        {
            _cellsFabric.FillWorld(_startCellPosition, _cellsCountOnX, _cellsCountOnY, _cellsCountOnZ);
            _environmentFabric.CreatePartsInRandomSequence(_startEnvPosition, _countEnvParts);
        }
        
        #endregion

        #region Остальные методы

        private bool IsCanReplaceObjectToCell(GameObjectBase obj, Cell cell)
        {
            var objCellsPositions = obj.CellsDescriptions.Select(d => d.CellPosition + obj.CellPosition);
            var cellPosDelta = cell.CellPosition - obj.CellPosition;
            var nearCells = _cellsFabric.GetCells(objCellsPositions.Select(p => p + cellPosDelta));
            return !nearCells.Any(c => c.HasObject);
        }

        public bool IsCanReplaceCharacterToCell(Character character, CellPosition position)
        {
            var cell = _cellsFabric.GetCell(position);
            return IsCanReplaceObjectToCell(character, cell);
        }

        /// <summary>
        /// Выполняет проверку возможности перемещения объекта, освобождает исходные клетки, занимает елевые клетки и перемещает объект на сцене.
        /// </summary>
        /// <param name="obj">Объект для перемещения.</param>
        /// <param name="target">Целевая позиция.</param>
        public void ReplaceObject(GameObjectBase obj, CellPosition target)
        {
            var cell = _cellsFabric.GetCell(target);
            var objCellsPositions = obj.CellsDescriptions.Select(d => d.CellPosition + obj.CellPosition);
            var cellPosDelta = cell.CellPosition - obj.CellPosition;
            var nearCells = _cellsFabric.GetCells(objCellsPositions.Select(p => p + cellPosDelta));

            if (!nearCells.Any(c => c.HasObject))
            {
                _cellsFabric.SetEmptyToCells(objCellsPositions);
                obj.CellPosition = cell.CellPosition;
                _cellsFabric.SetFillToCells(obj);
            }
        }

        public T Create<T>(T source, CellPosition position, Transform parent = null) where T : GameObjectBase
        {
            var result = _gameObjectFabric.CreateObject(source, parent);
            ReplaceObject(result, position);
            return result;
        }

        public T Create<T>(T source, Transform parent = null) where T : GameObjectBase
        {
            var result = _gameObjectFabric.CreateObject(source, parent);
            return result;
        }
        
        public int GetValidLaneNumber(int expectedNumber)
        {
            return Mathf.Clamp(expectedNumber, _firstRoadLaneNumber, _lastRoadLaneNumber);
        }

        /// <summary>
        /// Проверяет необходимость уничтожния объекта со сцены по его прохождению игроком.
        /// </summary>
        /// <param name="obj">Объект для проверки.</param>
        /// <returns>true, если игрок прошел объект на дистанцию _distanceToRemoveObjects, иначе false.</returns>
        public bool IsNeedDestroyObject(GameObjectBase obj)
        {
            return ReferenceAggregator.Player.CellPosition.Z > obj.CellPosition.Z + obj.DimensionByZ + _distanceToRemoveObjects;
        }

        public CellPosition GetClosestForwardCell(CellPosition currentPos, CellType cellType)
        {
            var truncPos = currentPos.Truncate();
            var cellsRow = _cellsFabric.GetCellsOnRowWithType((int)truncPos.Z + 1, cellType);
            var result = MathHelper.Min(cellsRow, c => (truncPos - c.CellPosition).Magnitude());
            return result.CellPosition;
        }
        
        #endregion
    }
}