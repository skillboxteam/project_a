using System.Collections.Generic;
using System.Linq;
using Game.Common;
using Game.Environment.Abstractions;
using Game.Environment.Cells.Description;
using UnityEngine;

namespace Game.Environment
{
    /// <summary>
    /// Фабрика элементов окружения.
    /// </summary>
    public class EnvironmentFabric : IEnvironmentFabric
    {
        private readonly GameObjectBase[] _partsPrefabs;
        
        private readonly Transform _parent;
        
        private readonly IGameObjectFabric _gameObjectFabric;

        private readonly ICellsFabric _cellsFabric;
        
        private readonly ICollection<GameObjectBase> _parts;
        
        public GameObjectBase CreatePart(GameObjectBase prefab, CellPosition position)
        {
            var result = _gameObjectFabric.CreateObject(prefab, _parent);
            result.CellPosition = position;
            _parts.Add(result);
            return result;
        }
        
        public GameObjectBase CreateRandomPart(CellPosition position)
        {
            var part = GetRandomPart();
            var result = CreatePart(part, position);
            return result;
        }

        public ICollection<GameObjectBase> CreatePartsInRandomSequence(CellPosition firstPos, int count)
        {
            _parts.Clear();
            var nextPositionOnZ = firstPos.Z;
            
            for (var i = 0; i < count; i++)
            {
                var part = CreateRandomPart(new CellPosition(firstPos.X, firstPos.Y, nextPositionOnZ));
                nextPositionOnZ += part.DimensionByZ;
                _parts.Add(part);
            }

            return _parts;
        }

        public ICollection<GameObjectBase> CreateParts(CellPosition firstPos, params GameObjectBase[] parts)
        {
            _parts.Clear();
            var nextPositionOnZ = firstPos.Z;
            
            foreach (var item in parts)
            {
                var part = CreatePart(item, new CellPosition(firstPos.X, firstPos.Y, nextPositionOnZ));
                nextPositionOnZ += part.DimensionByZ;
                _parts.Add(part);
            }
            return _parts;
        }

        private GameObjectBase GetRandomPart()
        {
            var index = Random.Range(0, _partsPrefabs.Length);
            return _partsPrefabs[index];
        }
        
        /// <summary>
        /// Инициализирует объект в памяти.
        /// </summary>
        /// <param name="gameObjectFabric">Фабрика игровых объектов.</param>
        /// <param name="cellsFabric">Фабрика клеток.</param>
        /// <param name="partsPrefabs">Список образцов участков окружения.</param>
        /// <param name="parent">Родительский объект.</param>
        public EnvironmentFabric(IGameObjectFabric gameObjectFabric, 
            ICellsFabric cellsFabric, 
            IEnumerable<GameObjectBase> partsPrefabs,
            Transform parent)
        {
            _partsPrefabs = partsPrefabs.ToArray();
            _gameObjectFabric = gameObjectFabric;
            _cellsFabric = cellsFabric;
            _parent = parent;
            _parts = new List<GameObjectBase>();
        }
    }
}