﻿using Game.Common;
using Game.Environment.Abstractions;
using UnityEngine;

namespace Game.Environment
{
    /// <summary>
    /// Фабрика игровых объектов.
    /// </summary>
    public class GameObjectFabric : IGameObjectFabric
    {
        /// <summary>
        /// Создает игровой объект на сцене.
        /// </summary>
        /// <typeparam name="T">GameObjectBase.</typeparam>
        /// <param name="source">Образец объекта.</param>
        /// <param name="parent">Родительский объект.</param>
        /// <returns>Ссылку на созданный объект.</returns>
        public T CreateObject<T>(T source, Transform parent = null) where T : MonoBehaviour
        {
            var result = Object.Instantiate(source, parent);
            return result;
        }

        /// <summary>
        /// Перемещает объект на сцене.
        /// </summary>
        /// <param name="obj">Ссылка на объект.</param>
        /// <param name="position">Новая позиция.</param>
        public void ReplaceObject(MonoBehaviour obj, Vector3 position)
        {
            obj.transform.position = position;
        }
    }
}
