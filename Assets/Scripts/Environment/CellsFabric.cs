﻿using Game.Environment.Abstractions;
using Game.Environment.Cells;
using Game.Environment.Cells.Description;
using System.Collections.Generic;
using System.Linq;
using Game.Common;
using Game.Common.Exceptions;
using Game.Common.Infrastructure;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Environment
{
    /// <summary>
    /// Фабрика клеток.
    /// </summary>
    public class CellsFabric : ICellsFabric
    {
        /// <summary>
        /// Образец клеток на сцене.
        /// </summary>
        private readonly Cell _prefab;
        
        /// <summary>
        /// Родительский объект на сцене для клеток.
        /// </summary>
        private readonly Transform _parent;

        /// <summary>
        /// Пометка, требуется ли отображать клетки на сцене.
        /// </summary>
        private readonly bool _isShowCellsOnScene;

        private readonly IGameObjectFabric _gameObjectFabric;
        
        /// <summary>
        /// Список клеток на сцене.
        /// </summary>
        private readonly ICollection<Cell> _cells;

        private Vector3 _firstCellWorldPosition;
        
        /// <summary>
        /// Создает пустую клетку.
        /// </summary>
        /// <param name="worldPosition">Позиция в мировой системе.</param>
        /// <param name="cellPosition">Позиция в клеточной системе.</param>
        /// <returns>Ссылку на созданную клетку.</returns>
        public Cell CreateEmptyCell(Vector3 worldPosition, CellPosition cellPosition)
        {
            var cell = _gameObjectFabric.CreateObject(_prefab, _parent);
            _gameObjectFabric.ReplaceObject(cell, worldPosition);

            if (!_isShowCellsOnScene)
                cell.Children[0].gameObject.SetActive(false);
            
            cell.Initialize(cellPosition, CellType.CanRun);
            _cells.Add(cell);
            return cell;
        }

        /// <summary>
        /// Заполняет мир клетками.
        /// </summary>
        /// <param name="firstCellWorldPosition">Позиция первой клетки в мировой системе координат.</param>
        /// <param name="cellsCountOnX">Количество клеток по оси X.</param>
        /// <param name="cellsCountOnY">Количество клеток по оси Y.</param>
        /// <param name="cellsCountOnZ">Количество клеток по оси Z.</param>
        /// <returns>Список ссылок на объекты клеток на сцене.</returns>
        public ICollection<Cell> FillWorld(Vector3 firstCellWorldPosition, int cellsCountOnX, int cellsCountOnY, int cellsCountOnZ)
        {
            _cells.Clear();
            Cell prevCell = null;
            _firstCellWorldPosition = firstCellWorldPosition;
            
            for (var x = 0; x < cellsCountOnX; x++)
            {
                for (var y = 0; y < cellsCountOnY; y++)
                {
                    for (var z = 0; z < cellsCountOnZ; z++)
                    {
                        var prevDim = prevCell?.Dimensions;
                        var newX = x * (prevDim?.x ?? 0) + firstCellWorldPosition.x;
                        var newY = y * (prevDim?.y ?? 0) + firstCellWorldPosition.y;
                        var newZ = z * (prevDim?.z ?? 0) + firstCellWorldPosition.z;
                        var cell = CreateEmptyCell(new Vector3(newX, newY, newZ),
                            new CellPosition(x, y, z));
                        prevCell = cell;
                    }
                }
            }

            return _cells;
        }

        /// <summary>
        /// Возвращает клетку по координатам.
        /// </summary>
        /// <param name="position">Координаты искомой клетки.</param>
        /// <returns>Ссылка на объект клетки.</returns>
        /// <exception cref="CellNotFoundException">Вызывается, если клетка не найдена.</exception>
        public Cell GetCell(CellPosition position)
        {
            var result = _cells.FirstOrDefault(c => c.Description.CellPosition.Equals(position));
            
            if(result == null)
                throw new CellNotFoundException(position);
            
            return result;
        }

        /// <summary>
        /// Возвращает список клеток по списку координат.
        /// </summary>
        /// <param name="positions">Список искомых координат.</param>
        /// <returns>Список ссылок на клетки.</returns>
        public IEnumerable<Cell> GetCells(IEnumerable<CellPosition> positions)
        {
            return positions.Select(GetCell);
        }

        public Vector3 GetWorldPositionByCellPosition(CellPosition position)
        {
            return position.ToVector3() + _firstCellWorldPosition;
        }

        public IEnumerable<Cell> GetCellsOnRowWithType(int rowNumber, CellType type)
        {
            var cellsRow = _cells.Where(c => c.Description.Type == type 
                                             && c.Description.CellPosition.Z == rowNumber);
            return cellsRow;
        }

        public void SetFillToCells(GameObjectBase obj)
        {
            var objCellsPositions = obj.CellsDescriptions.Select(d => new CellDescription(d.CellPosition + obj.CellPosition, d.Type));
            objCellsPositions.ForEach(cd => SetFillToCell(cd, obj));
        }
        
        public void SetFillToCell(CellDescription cellDescription, GameObjectBase obj)
        {
            var cell = GetCell(cellDescription.CellPosition);
            cell.Initialize(cell.CellPosition, cellDescription.Type, obj);
        }
        
        public void SetEmptyToCells(IEnumerable<CellPosition> cellPositions)
        {
            cellPositions.ForEach(SetEmptyToCell);
        }

        public void SetEmptyToCell(CellPosition cellPosition)
        {
            var cell = GetCell(cellPosition);
            cell.Initialize(cell.CellPosition, CellType.CanRun);
        }
        
        
        
        #region Конструктор

        /// <summary>
        /// Инициализирует объект в памяти.
        /// </summary>
        /// <param name="gameObjectFabric">Фабрика игровых объектов.</param>
        /// <param name="prefab">Образец клеток.</param>
        /// <param name="parent">Родительский объект на сцене для клеток.</param>
        /// <param name="isShowCellsOnScene">Пометка, требуется ли отображать клетки на сцене.</param>
        public CellsFabric(IGameObjectFabric gameObjectFabric, Cell prefab, Transform parent, bool isShowCellsOnScene)
        {
            _prefab = prefab;
            _parent = parent;
            _isShowCellsOnScene = isShowCellsOnScene;
            _gameObjectFabric = gameObjectFabric;
            _cells = new List<Cell>();
        }

        #endregion
    }
}