﻿using Game.Characters.Common;
using Game.Common;
using Game.Common.Infrastructure;
using UnityEngine;

namespace Game.UI
{
    /// <summary>
    /// Скрипт контроллера UI.
    /// </summary>
    public class UIController : MonoBehaviour
    {
        #region Поля

        /// <summary>
        /// Ссылка на скрипт полосы здоровья.
        /// </summary>
        [SerializeField]
        private HealthBar _healthBar = null;

        /// <summary>
        /// Ссылка на скрипт спидометра.
        /// </summary>
        [SerializeField]
        private Speedometer _speedometer = null;

        #endregion

        #region Свойства

        /// <summary>
        /// Ссылка на скрипт полосы здоровья.
        /// </summary>
        public HealthBar HealthBar => _healthBar;

        /// <summary>
        /// Ссылка на скрипт спидометра.
        /// </summary>
        public Speedometer Speedometer => _speedometer;
        
        #endregion
        
        /// <summary>
        /// Выполняет обновление полосы здоровья.
        /// </summary>
        private void UpdateHealthBar()
        {
            HealthBar.HealthCount = ReferenceAggregator.Player.CurHealth;
        }

        /// <summary>
        /// Выводит на экран текущую скорость.
        /// </summary>
        /// <param name="obj">Объект.</param>
        public void SetSpeed(MovableObjectBase obj)
        {
            Speedometer.Speed = obj.CurMoveSpeed;
        }

        /// <summary>
        /// Выполняет инициализацию объекта.
        /// </summary>
        private void Awake()
        {
            ReferenceAggregator.Player.OnHealthChanged += UpdateHealthBar;
            ReferenceAggregator.Player.OnCurMoveSpeedChanged += SetSpeed;
        }
    }
}
