﻿using TMPro;
using UnityEngine;

namespace Game.UI
{
    /// <summary>
    /// Скрипт спидометра.
    /// </summary>
    public class Speedometer : MonoBehaviour
    {
        /// <summary>
        /// Значение скорости.
        /// </summary>
        private float _speed = 0;

        /// <summary>
        /// Скрипт на ui элемент.
        /// </summary>
        private TextMeshProUGUI _speedometerText;

        /// <summary>
        /// Значение скорости.
        /// </summary>
        public float Speed
        {
            get => _speed;
            set
            {
                _speed = value;
                _speedometerText.text = _speed.ToString();
            }
        }

        /// <summary>
        /// Выполняет преинициализацию.
        /// </summary>
        private void Awake()
        {
            _speedometerText = GetComponentInChildren<TextMeshProUGUI>();
        }
    }
}