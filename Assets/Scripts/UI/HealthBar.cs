﻿using Game.Common;
using Game.Common.Infrastructure;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    /// <summary>
    /// Скрипт для полосы здоровья.
    /// </summary>
    public class HealthBar : MonoBehaviour
    {
        /// <summary>
        /// Ссылка на элемент UI.
        /// </summary>
        private Slider _slider;

        /// <summary>
        /// Количество здоровья.
        /// </summary>
        public float HealthCount
        {
            get => Slider.value;
            set => Slider.value = value;
        }

        /// <summary>
        /// Ссылка на элемент UI.
        /// </summary>
        private Slider Slider
        {
            get
            {
                if (_slider == null)
                {
                    _slider = GetComponent<Slider>();
                    _slider.minValue = 0;
                    _slider.maxValue = ReferenceAggregator.Player.MaxHealth;
                    _slider.value = ReferenceAggregator.Player.CurHealth;
                }

                return _slider;
            }
        }
    }
}