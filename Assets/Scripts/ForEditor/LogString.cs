﻿using UnityEngine;

namespace Game.ForEditor
{
    /// <summary>
    /// Класс для формирования логов.
    /// </summary>
    public class LogString
    {
        /// <summary>
        /// Ссылка на игровой объект, формирующий лог.
        /// </summary>
        private readonly GameObject _gameObject;

        /// <summary>
        /// Сообщение.
        /// </summary>
        private readonly string _message;
        
        public override string ToString()
        {
            return $"{_gameObject.name}: {_message}";
        }

        /// <summary>
        /// Инициализирует объект в памяти.
        /// </summary>
        /// <param name="gameObject">Игровой объект</param>
        /// <param name="message">Сообщение.</param>
        public LogString(GameObject gameObject, string message)
        {
            _gameObject = gameObject;
            _message = message;
        }
    }
}