﻿using System;
using UnityEngine;

namespace Game.Common.Exceptions
{
    /// <summary>
    /// Исключение, которое вызывается, если не найден компонент.
    /// </summary>
    public class ComponentNotFoundException : Exception
    {
        /// <summary>
        /// Инициализирует объект в памяти.
        /// </summary>
        /// <param name="source">Игровой объект - источник исключения.</param>
        /// <param name="componentType">Тип копонента,который не был найден.</param>
        public ComponentNotFoundException(GameObject source, Type componentType) 
            : base($"Компонент {componentType.Name} у объекта {source.name} не найден")
        {
        }
    }
}