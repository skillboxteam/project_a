﻿using System;
using Game.Environment.Cells.Description;

namespace Game.Common.Exceptions
{
    /// <summary>
    /// Исключение, которое вызывается, если не найдена клетка.
    /// </summary>
    public class CellNotFoundException : Exception
    {
        /// <summary>
        /// Инициализирует объект в памяти.
        /// </summary>
        /// <param name="pos">Координаты клеискомой клетки.</param>
        public CellNotFoundException(CellPosition pos) 
            : base($"Клетка с координатами {pos} не найдена")
        {
        }
    }
}