﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Common.Exceptions;
using Game.Common.Infrastructure;
using Game.Environment;
using Game.Environment.Abstractions;
using Game.Environment.Cells.Description;
using Game.ForEditor;
using UnityEngine;

namespace Game.Common
{
    /// <summary>
    /// Базовый класс для всех интерактивных объектов.
    /// </summary>
    public class GameObjectBase : MonoBehaviour
    {
        #region Поля

        /// <summary>
        /// Позиция нулевой занимаемой объектом клетки в общей (клеточной) системе координат.
        /// </summary>
        [SerializeField, ReadOnly] private CellPosition _cellPosition;

        /// <summary>
        /// Описание занимаемых клеток. Позиции в локальной системе координат.
        /// </summary>
        [SerializeField] private CellDescription[] _cellsDescriptions = null;

        #endregion

        #region Зависимости

        protected ICellsFabric CellsFabric => WorldConstructor.CellsFabric;

        #endregion

        #region Свойства

        /// <summary>
        /// Позиция объекта в клеточной системе координат.
        /// </summary>
        public CellPosition CellPosition
        {
            get => _cellPosition;
            set
            {
                _cellPosition = value;
                transform.position = CellsFabric.GetWorldPositionByCellPosition(value);
            }
        }

        /// <summary>
        /// Описание занимаемых клеток. Позиции в локальной системе координат.
        /// </summary>
        public IEnumerable<CellDescription> CellsDescriptions => _cellsDescriptions;
        // {
        //     // get
        //     // {
        //     //     // return _cellsDescriptions.Select(item => new CellDescription
        //     //     // {
        //     //     //     CellPosition = item.CellPosition + CellPosition,
        //     //     //     Type = item.Type
        //     //     // });
        //     // }
        // }

        /// <summary>
        /// Размерность объекта по оси X.
        /// </summary>
        public float DimensionByX
        {
            get
            {
                if (_cellsDescriptions.Any())
                {
                    var minX = _cellsDescriptions.Min(d => d.CellPosition.X);
                    var maxX = _cellsDescriptions.Max(d => d.CellPosition.X);
                    return maxX - minX + 1;
                }

                return 0;
            }
        }

        /// <summary>
        /// Размерность объекта по оси Y.
        /// </summary>
        public float DimensionByY
        {
            get
            {
                if (_cellsDescriptions.Any())
                {
                    var minY = _cellsDescriptions.Min(d => d.CellPosition.Y);
                    var maxY = _cellsDescriptions.Max(d => d.CellPosition.Y);
                    return maxY - minY + 1;
                }

                return 0;
            }
        }

        /// <summary>
        /// Размерность объекта по оси Z.
        /// </summary>
        public float DimensionByZ
        {
            get
            {
                if (_cellsDescriptions.Any())
                {
                    var minZ = _cellsDescriptions.Min(d => d.CellPosition.Z);
                    var maxZ = _cellsDescriptions.Max(d => d.CellPosition.Z);
                    return maxZ - minZ + 1;
                }

                return 0;
            }
        }

        /// <summary>
        /// Идентификатор объекта на сцене.
        /// </summary>
        public Guid IdOnScene { get; private set; }

        /// <summary>
        /// Менеджер клеток сцены.
        /// </summary>
        protected static WorldConstructor WorldConstructor => ReferenceAggregator.WorldConstructor;

        #endregion

        #region Методы жизненного цикла

        /// <summary>
        /// Выплняет преинициализацию объекта.
        /// </summary>
        protected virtual void Awake()
        {
            IdOnScene = Guid.NewGuid();
        }

        /// <summary>
        /// Выполняет инициализацию объекта.
        /// </summary>
        protected virtual void Start()
        {
            if (!_cellsDescriptions.Any())
                Debug.LogWarning(new LogString(gameObject,
                    "Описание занимаемых клеток не заполнено. Могут возникнуть ошибки."));
        }

        protected virtual void Update()
        {
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
        }

        protected virtual void OnTriggerExit(Collider other)
        {
        }
        
        #endregion

        #region Остальные методы

        /// <summary>
        /// Ищет и возращает дочерний ссылку на объект на сцене с именем name.
        /// </summary>
        /// <param name="childName">Имя объекта.</param>
        /// <returns>Transform дочернего объекта.</returns>
        public Transform FindChild(string childName)
        {
            return transform.Find(childName);
        }

        /// <summary>
        /// Возвращает дочерние объекты на сцене.
        /// </summary>
        /// <returns>Список дочерних элементов.</returns>
        public IEnumerable<Transform> GetChildren()
        {
            return transform.Cast<Transform>().ToList();
        }

        /// <summary>
        /// Возвращает дочерний объект на сцене с тегом tag.
        /// </summary>
        /// <param name="childTag">Искомый тег.</param>
        /// <returns>Возвращает дочерний элемент; null, если tag - пустая строка или объект не найден.</returns>
        public Transform GetChildWithTag(string childTag)
        {
            if (string.IsNullOrEmpty(childTag))
                return null;

            return GetChildren().FirstOrDefault(c => c.CompareTag(childTag));
        }

        #endregion
    }
}