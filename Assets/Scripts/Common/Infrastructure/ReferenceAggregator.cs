﻿using Game.Characters;
using Game.Controllers;
using Game.Environment;
using Game.Effects;
using Game.UI;
using UnityEngine;

namespace Game.Common.Infrastructure
{
    /// <summary>
    /// Агрегатор ссылок.
    /// </summary>
    public class ReferenceAggregator : MonoBehaviour
    {
        public static WorldConstructor WorldConstructor { get; private set; }

        /// <summary>
        /// Игрок.
        /// </summary>
        public static Player Player { get; private set; }

        /// <summary>
        /// Игровой контроллер.
        /// </summary>
        public static GameController GameController { get; private set; }

        /// <summary>
        /// Ссылка на контроллер ui.
        /// </summary>
        public static UIController UiController { get; private set; }

        /// <summary>
        /// Ссылка на контроллер эффектов.
        /// </summary>
        public static EffectManager EffectController { get; private set; }

        /// <summary>
        /// Выполняет преинициализацию объекта.
        /// </summary>
        private void Awake()
        {
            Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            var objManager = GameObject.FindGameObjectWithTag("ObjectsManager");
            WorldConstructor = objManager.GetComponent<WorldConstructor>();
            GameController = objManager.GetComponent<GameController>();
            var uiObj = GameObject.FindGameObjectWithTag("UIRoot");
            UiController = uiObj.GetComponent<UIController>();
            EffectController = GameObject.FindGameObjectWithTag("EffectRoot").GetComponent<EffectManager>();
        }
    }
}