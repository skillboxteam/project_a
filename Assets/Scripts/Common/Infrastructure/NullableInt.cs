﻿using System;
using UnityEngine;

namespace Game.Common.Infrastructure
{
    /// <summary>
    /// Обертка над int? Для сериализации в движке.
    /// </summary>
    [Serializable]
    public struct NullableInt
    {
        /// <summary>
        /// Пометка, имеется ли значение.
        /// </summary>
        [SerializeField]
        private bool _hasValue;

        /// <summary>
        /// Значение.
        /// </summary>
        [SerializeField]
        private int _value;

        /// <summary>
        /// Значение.
        /// </summary>
        public int? Value
        {
            get
            {
                if (_hasValue)
                    return _value;
                else
                    return null;
            }
        }

        /// <summary>
        /// Инициализирует объект в памяти.
        /// </summary>
        /// <param name="value">Исходное значение.</param>
        public NullableInt(int? value)
        {
            if (value.HasValue)
            {
                _hasValue = true;
                _value = value.Value;
            }
            else
            {
                _hasValue = false;
                _value = 0;
            }
        }

        /// <summary>
        /// Перегрузка операции неявного приведения.
        /// </summary>
        /// <param name="value">Значение типа NullableInt.</param>
        public static implicit operator int?(NullableInt value)
        {
            return value.Value;
        }
    }
}