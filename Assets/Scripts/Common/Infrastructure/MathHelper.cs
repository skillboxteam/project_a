﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Common.Infrastructure
{
    /// <summary>
    /// Хранит вспомогательные математические функции.
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// Возвращает значение по формуле квадратичной кривой Безье.
        /// </summary>
        /// <param name="start">Стартовое значение.</param>
        /// <param name="middle">Среднее опорное значение.</param>
        /// <param name="end">Итоговое значение.</param>
        /// <param name="t">Шаг расчета. Может хранить значение от 0 до 1.</param>
        /// <returns>Значение по формуле квадратичной кривой Безье на на шагу t.</returns>
        public static float Bezier(float start, float middle, float end, float t)
        {
            if (t <= 0)
                return start;

            if (t >= 1)
                return end;

            var result = (start * ((1 - t) * (1 - t))) + (middle * 2 * t * (1 - t)) + (end * (t * t));
            return result;
        }

        /// <summary>
        /// Возвращает объект из коллекции с минимальным selector.
        /// </summary>
        /// <typeparam name="T">Любой тип.</typeparam>
        /// <typeparam name="TResult">Должен реальзовать IComparable.</typeparam>
        /// <param name="collection">Коллекция объектов.</param>
        /// <param name="selector">Селектор выборки.</param>
        /// <returns>Минимальный объект.</returns>
        public static T Min<T, TResult>(IEnumerable<T> collection, Func<T, TResult> selector) where TResult : IComparable
        {
            if (!collection.Any())
                return default;

            if (selector == null)
                throw new ArgumentNullException(nameof(selector));

            T min = collection.FirstOrDefault();
            var minValue = selector.Invoke(min);

            foreach (var item in collection)
            {
                var value = selector.Invoke(item);

                if (value.CompareTo(minValue) <= -1)
                {
                    min = item;
                    minValue = value;   
                }
            }

            return min;
        }

        /// <summary>
        /// Отбрасывает десятичную часть числа (окугление до ближайшего целого в меньшую сторону).
        /// </summary>
        /// <param name="value">Исходное значение.</param>
        /// <returns>Округленное число без десятичной части.</returns>
        public static float Truncate(float value)
        {
            return (int)value;
        }
    }
}