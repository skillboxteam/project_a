﻿using Game.Effects;
using NUnit.Framework;

namespace Tests
{
    public class SpeedEffectTest
    {
        private static SpeedStats stats = null;

        [SetUp]
        public void Setup()
        {
            stats = new SpeedStats()
            {
                CurMoveSpeed = 10,
                NormalMoveSpeed = 10,
                MaxMoveSpeed = 30,
                MinMoveSpeed = 5
            };
        }

        #region SpeedMayChange
        [Test]
        public void SpeedMayChange_NormalSpeed_ResultTrue()
        {
            // Assign
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = 5;
            var expectedValue = true;

            // Act
            var result = calculator.SpeedMayChange(delta);

            // Assert
            Assert.AreEqual(expectedValue, result);
        }

        [Test]
        public void SpeedMayChange_MinSpeedAndNegativeDelta_ResultFalse()
        {
            // Assign
            stats.CurMoveSpeed = stats.MinMoveSpeed;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = -5;
            var expectedValue = false;

            // Act
            var result = calculator.SpeedMayChange(delta);

            // Assert
            Assert.AreEqual(expectedValue, result);
        }

        [Test]
        public void SpeedMayChange_MaxSpeedAndPositiveDelta_ResultFalse()
        {
            // Assign
            stats.CurMoveSpeed = stats.MaxMoveSpeed;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = 5;
            var expectedValue = false;

            // Act
            var result = calculator.SpeedMayChange(delta);

            // Assert
            Assert.AreEqual(expectedValue, result);
        }
        #endregion

        #region CalculateDelta
        [Test]
        public void CalculateDelta_FactorFifty_ResultFive()
        {
            // Assign
            var factor = 50;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var expectedDeltaValue = 5;

            // Act
            var result = calculator.CalculateDelta(factor);

            // Assert
            Assert.AreEqual(expectedDeltaValue, result);
        }
        #endregion

        #region NormalizeDelta

        /// <summary>
        /// Если скорость после применения дельты будет меньше минимальной
        /// меняем дельту на допустимую.
        /// </summary>
        [Test]
        public void NormalizeDelta_MinSpeedAndNegativeFactor_ResultZero()
        {
            // Assign
            var factor = -20;
            stats.CurMoveSpeed = 6;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = calculator.CalculateDelta(factor);
            var expectedDeltaValue = -1;

            // Act
            var result = calculator.NormalizeDelta(delta);

            // Assert
            Assert.AreEqual(expectedDeltaValue, result);
        }

        /// <summary>
        /// Если скорость после применения дельты будет больше максимальной
        /// меняем дельту на допустимую.
        /// </summary>
        [Test]
        public void NormalizeDelta_MaxSpeedAndPositiveFactor_ResultZero()
        {
            // Assign
            var factor = 60;
            stats.CurMoveSpeed = 25;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = calculator.CalculateDelta(factor);
            var expectedDeltaValue = 5;

            // Act
            var result = calculator.NormalizeDelta(delta);

            // Assert
            Assert.AreEqual(expectedDeltaValue, result);
        }

        [Test]
        public void NormalizeDelta_FactorMinus60Speed10_ResultMinusFive()
        {
            // Assign
            var factor = -60;
            stats.CurMoveSpeed = 10;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = calculator.CalculateDelta(factor);
            var expectedDeltaValue = -5;

            // Act
            var result = calculator.NormalizeDelta(delta);

            // Assert
            Assert.AreEqual(expectedDeltaValue, result);
        }

        [Test]
        public void NormalizeDelta_FactorMinus30Speed10_ResultMinusThree()
        {
            // Assign
            var factor = -30;
            stats.CurMoveSpeed = 10;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = calculator.CalculateDelta(factor);
            var expectedDeltaValue = -3;

            // Act
            var result = calculator.NormalizeDelta(delta);

            // Assert
            Assert.AreEqual(expectedDeltaValue, result);
        }

        [Test]
        public void NormalizeDelta_FactorMinus30SpeedMinimum_ResultZero()
        {
            // Assign
            var factor = -30;
            stats.CurMoveSpeed = stats.MinMoveSpeed;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = calculator.CalculateDelta(factor);
            var expectedDeltaValue = 0;

            // Act
            var result = calculator.NormalizeDelta(delta);

            // Assert
            Assert.AreEqual(expectedDeltaValue, result);
        }

        [Test]
        public void NormalizeDelta_Factor30SpeedMaximum_ResultZero()
        {
            // Assign
            var factor = 30;
            stats.CurMoveSpeed = stats.MaxMoveSpeed;
            SpeedCalculator calculator = new SpeedCalculator(stats);
            var delta = calculator.CalculateDelta(factor);
            var expectedDeltaValue = 0;

            // Act
            var result = calculator.NormalizeDelta(delta);

            // Assert
            Assert.AreEqual(expectedDeltaValue, result);
        }
        #endregion
    }
}
